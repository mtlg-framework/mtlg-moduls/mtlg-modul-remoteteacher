
MTLG.lang.define({
  'en': {
    'one_player': 'One Player',
    'two_player': 'Two Players',
    'title_login': 'Type in User and password',
    'title_field1': 'Take the rectangle to the white area',
    'title_field2': 'Press the fields with the numbers in the right order...',
    'temp_feedback': 'Your needed time in milliseconds is: ',
    'feedback_text': 'ms needed',
    'demo_game' : 'Demo Game',
    'start!' : 'Start!',
    'return_to_menu' : 'Return to Menu',
  },
  'de': {
    'one_player': 'Ein Spieler',
    'two_player': 'Zwei Spieler',
    'title_login': 'Bitte Namen und Passwort eingeben',
    'title_field1': 'Ziehe das Rechteck auf das weiße Feld',
    'title_field2': 'Drücke die Nummern in der richtigen Reihenfolge...',
    'temp_feedback': 'Deine benötigte Zeit (in ms) beträgt: ',
    'feedback_text': 'ms gebraucht',
    'demo_game' : 'Demo Game',
    'start!' : 'Start!',
    'return_to_menu' : 'Zurück zum Menü',
  }
});
