/*
 * Demo game to debug the functions of the remoteteacher module.
 */

// expose framework for debugging
window.MTLG = MTLG;
window.window.createjs = createjs;
// ignite the framework
document.addEventListener("DOMContentLoaded", MTLG.init);

// add modules
require('mtlg-modul-menu');
require('mtlg-moduls-utilities');
require('mtlg-modul-lightparse');
require('mtlg-module-dd');
require('mtlg-module-remoteteacher');

// load configuration
require('../manifest/device.config.js');
require('../manifest/game.config.js');
require('../manifest/game.settings.js');

MTLG.addGameInit(() => {

  // menu
  MTLG.lc.registerMenu(() => {

    {
      let i = 0;
      var createLevelButton = (text, callback) => MTLG.utils.uiElements.addButton(
        { text, sizeX: 200, sizeY: 70, bgColor2: "red" }, callback)
        .set({x:.5*MTLG.getOptions().width, y:(.3+i++*.1)*MTLG.getOptions().height});
    }

    {
      let joinRoom = (callback) => {
        MTLG.distributedDisplays.rooms.joinRoom('RT_tabula', result => {
          if (result && result.success) {
            console.log('successfully joined classroom');
            callback();
          } else console.warn('failed to join classroom: '+result.reason)
        })
      }
      var createJoinRoom = (callback) => {
        // create room before joining
        MTLG.distributedDisplays.rooms.getAllRooms(rooms => {
          if(rooms['RT_tabula']) return joinRoom(callback)
          MTLG.distributedDisplays.rooms.createRoom('RT_tabula', result => {
            if(result && result.success) joinRoom(callback)
            else console.warn('failed to create classroom: '+result.reason)
          })
        })
      }
    }

    MTLG.menu.start();
    MTLG.setBackgroundColor('#abc');
    MTLG.getStageContainer().addChild(
      // standard pupil
      createLevelButton("standard pupil", () => {
        MTLG.remoteteacher.init({remoteteacher:{teacherDefaultScreen:true}}); // default
        // invoke room selection
        MTLG.lc.levelFinished({ nextLevel: 0 })
      }),
      // standard teacher
      createLevelButton("standard teacher", () => {
        MTLG.remoteteacher.teacherView.drawRoomHandler()
      }),
      // custom pupil
      createLevelButton("custom pupil", () => {
        MTLG.remoteteacher.init({remoteteacher:{teacherDefaultScreen:false}})
        // TODO: via timeout is bad, a hook should be provided by ddd
        setTimeout(() => createJoinRoom(() => {
          MTLG.remoteteacher.playerView.init('RT_tabula')
          MTLG.remoteteacher.playerView.forSharing(MTLG.getStage());
          MTLG.lc.levelFinished({ nextLevel: "level1" })
        }), 1000)
      }),
      // custom teacher
      createLevelButton("custom teacher", () => {
        MTLG.remoteteacher.init({remoteteacher:{teacherDefaultScreen:false}})
        // TODO: via timeout is bad, a hook should be provided by ddd
        setTimeout(() => createJoinRoom(() => {
          MTLG.remoteteacher.teacherView.init('RT_tabula')
          MTLG.remoteteacher.teacherView.draw();
        }), 1000)
      })
    );
  });

  // go to level1 after room selection
  MTLG.lc.registerLevel(
    () => MTLG.lc.levelFinished({nextLevel:'level1'}),
    state => state.nextLevel == 1
  );

  // level 1
  MTLG.lc.registerLevel(
    () => {
      let metadata = { "Moves": 0 };
      MTLG.remoteteacher.playerView.setMetadata(metadata);

      let createRect = (color, w, h) => {
        let rect = MTLG.utils.gfx.getShape();
        rect.graphics.beginFill(color).drawRect(0, 0, w, h);
        return rect
      }
      let background = createRect('blue', MTLG.getOptions().width, MTLG.getOptions().height);
      let zone = createRect('white', 200, 200).set({x:100, y:200});
      let object = createRect('yellow', 50, 50).set({x:900, y:500});
      object.on('pressmove', e => object.set( object.localToLocal(e.localX, e.localY, object.parent) ) );
      object.on('pressup', () => {
        metadata['Moves']++;

        let relPos = object.localToLocal(0, 0, zone);
        if(0<relPos.x && relPos.x<200 && 0<relPos.y && relPos.y<200) {
          object.removeAllEventListeners();
          MTLG.lc.levelFinished({nextLevel: "level2"});
        }
      });

      MTLG.getStageContainer().addChild(background, zone, object);
    }, (state) => state.nextLevel == "level1"
  );

  // level 2
  MTLG.lc.registerLevel(
    () => {
      MTLG.remoteteacher.playerView.setMetadata({"Level": 2});

      MTLG.getStageContainer().addChild(
        MTLG.utils.uiElements.addButton(
          { text: "win!", sizeX: 200, sizeY: 70, bgColor2: "red" },
          () => MTLG.lc.goToMenu()
        ).set({x:.5*MTLG.getOptions().width, y:.5*MTLG.getOptions().height})
      )
    }, (state) => state.nextLevel == "level2"
  );
});
