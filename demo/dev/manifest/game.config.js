MTLG.loadOptions({
  "width":1920, //game width in pixels
  "height":1080, //game height in pixels
  "languages":["en","de"], //Supported languages. First language should be the most complete.
  "countdown":180, //idle time countdown
  "fps":"60", //Frames per second
  "playernumber":4, //Maximum number of supported players
  "FilterTouches": true, //Tangible setting: true means a tangible is recognized as one touch event. False: 4 events.
  "webgl": false, //Using webgl enables using more graphical effects
  "dd": {
    "host": "localhost",
    "port": 3125
  },
  'remoteteacher': {
    'teacherDefaultScreen': false
  }
});
