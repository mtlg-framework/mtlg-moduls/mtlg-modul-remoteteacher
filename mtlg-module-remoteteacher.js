/**
 * @Date:   2018-06-01T22:14:49+02:00
 * @Last modified time: 2018-08-03T14:36:44+02:00
 */

/**
 * This remote teacher module enables virtual class-room creation, monitoring, realtime remote drawing on a pupil's screen.
 *
 * @author Rabea de Groot
 * @namespace remoteteacher
 * @memberof MTLG
 **/

const swal = require('sweetalert2');
require.context('./img');

var config = {
  /* teacherTools are tools for the active intervention */
  teacherTools: {
    feedback: true, //choose if the teacher should be able to give feedback
    drawing: true //choose if the teacher should be able to draw onto the playing field
  },
  teacherToolsOptions: {
    drawing: {
      colors: ['black', 'white', 'red', 'blue', 'green', 'yellow'],
      sizes: ['1', '5', '7', '10', '15', '20']
    }
  },
  numberOfPlayersForMultiObserver: 9, //the number of player sessions, which should be shown at the same time
  teacherDefaultScreen: true, //true, if the default login should be used
};

var l = MTLG.lang.getString; //shortcut for language strings
//Define the specific language strings for remote teacher
MTLG.lang.define({
  'en': {
    'rt_help_rotation': 'The rotation of the information box',
    'rt_help_content': 'The content of the information box',
    'rt_help_totalDuration': 'Time in seconds, how long the box should be available.',
    'rt_help_openDuration': 'Time in seconds, how long the box should be opened.',
    'rt_help_miniDuration': 'Time in seconds, how long the box should be minimized.',
    'rt_help_theme': 'Theme of the information box. At the moment "Dard" and "Light" are available.',
    'rt_help_buttonColor': 'The color of the buttons e.g. "#FFFFFF", "white", "rgb(255,255,255)"',
    'rt_return_to_menu': 'Return to Menu',
    'rt_field1_addRoomErrorTitle': 'Room creation failed',
    'rt_field1_addRoomPlaceholder': 'Room name',
    'rt_field1_addRoomQuestion': 'Please enter the name of the room to add:',
    'rt_field1_addRoomTitle': 'Add a new room',
    'rt_error:unknown': 'An unknown error has occured',
    'rt_title:error': 'Error!',
    'rt_error:noNameForRoom': 'The room has to have a name.',
    'rt_error:swal': 'An error has occured during the use of SweetAlert2. Check, if you are using the right version.',
    'rt_error:imageNotFound': 'The following picture could not be found: ',
    'rt_refresh': 'Refresh',
    'rt_showAll': 'Show all',
    'rt_joinRoom': 'Join a Room',
    'rt_title:errorCreatingRoom': 'Error creating a room',
    'rt_createRoom': 'Create a Room',
    'rt_error:creatingRoom': 'An error has occured during the creation of a room.',
    'rt_noRoomsAvailable': 'Currently there are no rooms available!',
    'rt_error:getPlayers': 'An error occured while loading the players.',
    'rt_title:noRespondOfPlayer': 'Error!',
    'rt_error:noRespondOfPlayer': 'No response! Check the internet connection and try again.',
    'rt_title:DD': 'Error in Distributed Display Module',
    'rt_errorText:addSharedObjects': 'An error has occured while adding a SharedObject.',
    'rt_error:addSharedObjects': 'Error adding a SharedObject',
    'rt_warn:nothingShared': 'Currently there is no element transfert by the client.',
    'rt_error:getAllRooms': 'An error has occured while loading the rooms.',
    'rt_error:noDDM': 'The DistributedDisplay-Module was not included. For using the Remote-Teacher Module you have to include the DistributedDisplay-Module!',
    'rt_feedback': 'Feedback',
    'rt_next': 'Next &rarr;',
    'rt_cancel': 'Cancel',
    'rt_rotation': 'Rotation',
    'rt_movable': 'movable',
    'rt_movableMini': 'movable in minimized state',
    'rt_rotateOnMove': 'rotate on move',
    'rt_minimizable': 'minimizable',
    'rt_closeable': 'closable',
    'rt_content': 'content',
    'rt_totalDisplaytime': 'How long should the feedback be displayed total?',
    'rt_openDisplaytime': 'How long should the feedback be open?',
    'rt_miniDisplaytime': 'How long should the feedback be minimized?',
    'rt_theme': 'Theme',
    'rt_buttonColor': 'color of buttons',
    'rt_pickPointForFeedback': 'Click any point to place the feedback there.',
    'rt_error:feedbackDialog': 'An error has occured in the feedback window.',
    'rt_title:feedbackSent': 'Sent!',
    'rt_text:feedbackSent': 'The feedback was sent successfully. The client should see it now.',
    'rt_error:feedbackSent': 'An error occured while sending the feedback.',
    'rt_title:feedbackError': 'Error!',
    'rt_drawing': 'Drawing',
  },
  'de': {
    'rt_help_rotation': 'Die Rotation gibt an, wie die Hinweisbox gedreht sein soll. Angabe erfolgt in Grad (0-360)',
    'rt_help_content': 'Inhalt der Hinweisbox',
    'rt_help_totalDuration': 'Zeit in Sekunden, wie lange die Hinweisbox insgesamt angezeigt werden soll. Bei einer Angabe von 0, bleibt die Hinweisbox dauerhaft eingeblendet.',
    'rt_help_openDuration': 'Zeit in Sekunden, wie lange die Hinweisbox geöffnet bleiben darf.',
    'rt_help_miniDuration': 'Zeit in Sekunden, wie lange die Hinweisbox minimirt sein soll.',
    'rt_help_theme': 'Das Theme der Hinweisbox. Verfügbar sind bisher "Dark" und "Light".',
    'rt_help_buttonColor': 'Die Farbe der Buttons, z.B. "#FFFFFF", "white", "rgb(255,255,255)"',
    'rt_field1_addRoomErrorTitle': 'Raum konnte nicht erstellt werden',
    'rt_field1_addRoomPlaceholder': 'Raum Name',
    'rt_field1_addRoomQuestion': 'Bitte den Namen des zu erstellenden Raumes eingeben:',
    'rt_field1_addRoomTitle': 'Neuen Raum erstellen',
    'rt_error:unknown': 'Ein unbekannter Fehler ist aufgetreten',
    'rt_title:error': 'Fehler!',
    'rt_error:noNameForRoom': 'Es muss ein Name für den Raum angegeben werden.',
    'rt_error:swal': 'Es ist ein Fehler bei der Verwendung von SweetAlert2 aufgetreten. Prüfe, ob die richtige Version eingebunden ist.',
    'rt_error:imageNotFound': 'Folgendes Bild konnte nicht gefunden werde: ',
    'rt_refresh': 'Aktualisieren',
    'rt_showAll': 'Überwachungsmonitor',
    'rt_joinRoom': 'Raum betreten',
    'rt_title:errorCreatingRoom': 'Fehler beim Erstellen eines Raums',
    'rt_createRoom': 'Raum erstellen',
    'rt_error:creatingRoom': 'Beim Erstellen eines Raumes ist ein Fehler aufgetreten.',
    'rt_noRoomsAvailable': 'Es sind momentan keine Räume verfügbar!',
    'rt_error:getPlayers': 'Beim Laden der Spielenden ist ein Fehler aufgetreten.',
    'rt_title:noRespondOfPlayer': 'Fehler!',
    'rt_error:noRespondOfPlayer': 'Keine Anwort! Überprüfe die Internetverbindung und versuche es erneut.',
    'rt_title:DD': 'Fehler im Distributed Display Module',
    'rt_errorText:addSharedObjects': 'Beim Hinzufügen eines SharedObjects ist ein Fehler aufgetreten.',
    'rt_error:addSharedObjects': 'Fehler beim Hinzufügen eines SharedObjects.',
    'rt_warn:nothingShared': 'Momentan wird vom Client kein Element übertragen. ',
    'rt_error:getAllRooms': 'Fehler beim Laden der Räume aufgetreten.',
    'rt_error:noDDM': 'Das DistributedDisplay-Module wurde nicht eingebunden. Für die Remote-Teacher Komponente muss das DistributedDisplay-Module eingebunden werden!',
    'rt_feedback': 'Feedback',
    'rt_next': 'Weiter &rarr;',
    'rt_cancel': 'Abbrechen',
    'rt_rotation': 'Rotation',
    'rt_movable': 'bewegbar',
    'rt_movableMini': 'im minimierten Zustand bewegbar',
    'rt_rotateOnMove': 'beim Bewegen rotieren',
    'rt_minimizable': 'minimierbar',
    'rt_closeable': 'schließbar',
    'rt_content': 'Inhalt',
    'rt_totalDisplaytime': 'Wie lange soll insgesamt das Feedback angezeigt werden?',
    'rt_openDisplaytime': 'Wie lange soll das Feedback offen sein?',
    'rt_miniDisplaytime': 'Wie lange soll das Feedback minimiert sein?',
    'rt_theme': 'Theme',
    'rt_buttonColor': 'Farbe der Buttons',
    'rt_pickPointForFeedback': 'Klicken Sie ein beliebigen Punkt, um dort das Feedback zu platzieren',
    'rt_error:feedbackDialog': 'Es ist ein Fehler im Feedbackfenster aufgetreten.',
    'rt_title:feedbackSent': 'Gesendet!',
    'rt_text:feedbackSent': 'Das Feedback wurde erfolgreich versendet. Es sollte nun bei den Spielenden angezeigt werden.',
    'rt_error:feedbackSent': 'Beim Senden des Feedbacks ist ein Fehler aufgetreten.',
    'rt_title:feedbackError': 'Fehler!',
    'rt_drawing': 'Zeichnen',
  }
});


var toolbarFunctions = new Map(); //Map of all functions for active intervention

var _width;
var _height;

var teacherRoomPrefix = "RT_"; //Prefix for filtering


/*
 * This function is for joining an existing room.
 * @param {object} event The click event
 * @param {object} data The data for the function. This has to be an object with at least the name of the room, which should be joined and a callback.
 * {name: nameOfRoom, callback: (result) => ()}
 */
var joinRoomHandler = function(event, data) {
  MTLG.distributedDisplays.rooms.joinRoom(data.name, function(result) {
    if (result && result.success) {
      data.callback(result);
    } else {
      if (result.reason) {
        swal({
          type: 'error',
          title: l("rt_field1_joinRoomErrorTitle"),
          text: result.reason,
          focusConfirm: true
        });
      } else {
        console.error(l('rt_error:unknown'));
      }
    }
  });
};

/*
 * createRoom - This function opens a dialogue, where you can enter a name for the new room. A room with the given name will be created.
 *
 * @return {Promise}          returns a promise with the result of the creation of the room {success: true, name: nameOfTheRoom}.
 */
var createRoom = function() {
  try {
    return swal({
      type: 'question',
      title: l("rt_field1_addRoomTitle"),
      text: l("rt_field1_addRoomQuestion"),
      input: 'text',
      inputPlaceholder: l("rt_field1_addRoomPlaceholder"),
      allowEnterKey: true,
      focusConfirm: false,
      showCancelButton: true,
      inputValidator: function(value) {
        return new Promise(function(resolve, reject) {
          if (value) {
            resolve();
          } else {
            reject({
              noRoom: true
            });
          }
        });
      }
    }).then(function(result) {
      return new Promise(function(resolve, reject) {
        if (result.dismiss) resolve(result);
        else {
          let name = result.value;
          MTLG.distributedDisplays.rooms.createRoom(teacherRoomPrefix + name, function(result) {
            if (result && result.success) {
              resolve(result);
            } else {
              reject(result.reason);
            }
          });
        }
      });
    }, function(err) {
      swal({
        type: 'error',
        title: l('rt_title:error'),
        text: l('rt_error:noNameForRoom')
      });
    });
  } catch (e) {
    console.error(l('rt_error:swal'), e);
  }
};


/*
 * resizeChildren - This function scales all children of the given element with the global scalefactor of MTLG
 *
 * @param  {createJsObject} parent Element, whose children are to be scaled
 */
var resizeChildren = function(parent) {
  var scaleFactor = MTLG.getScaleFactor();
  for (var index in parent.children) {
    var child = parent.children[index];
    var oldScale = child.scaleX < child.scaleY ? child.scaleX : child.scaleY;
    child.scaleX = scaleFactor;
    child.scaleY = scaleFactor;
    child.x = child.x / oldScale * scaleFactor;
    child.y = child.y / oldScale * scaleFactor;
  }
}


/**
 * This function resizes the teacherToolbarStage.
 * @memberof MTLG.remoteteacher#
 */
var resize = function() {
  var stage = teacherView.getToolbarStage();
  if (stage) {
    var canvas = stage.canvas;
    let scaleX = window.innerWidth / MTLG.getOptions().width;
    let scaleY = window.innerHeight / MTLG.getOptions().height;
    if (scaleX < scaleY) {
      canvas.width = window.innerWidth;
      canvas.height = MTLG.getOptions().height * scaleX;
    } else {
      canvas.width = MTLG.getOptions().width * scaleY;
      canvas.height = window.innerHeight;
    }

    resizeChildren(stage);
  }
}

/*
 * createButton - Creates a new button like element, inline text button
 *
 * @param  {number} h             The height of the button, its width is calculated from the text width, except of the minWidth
 * @param  {string} text          The text to display on the button
 * @param  {string} color = "red" The color of the button. A CSS compatible color value.
 * @param  {string} textColor = "red" The color of the text. A CSS compatible color value.
 * @param  {number} minWidth = 50 The minium width of the button
 * @return {createJS.Container}   Returns the created button
 */
var createButton = function(h, text, color = "blue", textColor = "white", minWidth = 50) {
  var buttonContainer = new createjs.Container();
  var textObj = new createjs.Text(text, '12px Arial', textColor);
  textObj.textBaseline = 'middle';
  textObj.textAlign = 'center';
  var bounds = textObj.getBounds();
  //determine if the textObj width is greater than the minimum width
  if (minWidth && (minWidth > (bounds.width + h))) bounds.width = minWidth - h;
  textObj.x = (bounds.width + h) / 2; // Width of text + padding
  textObj.y = h / 2;

  var back = new createjs.Shape();
  back.name = "back";
  back.graphics.beginFill(color).drawRoundRect(0, 0, bounds.width + h, h, h / 10);

  buttonContainer.addChild(back);
  buttonContainer.addChild(textObj); // Text should be the second child
  buttonContainer.setBounds(0, 0, bounds.width + h, h);
  return buttonContainer;
};

/*
 * createButton - Creates a new button like element with an image inside
 *
 * @param  {number} w             The width of the button
 * @param  {number} h          The height of the button
 * @param  {string} img         Source of image
 * @return {createJS.Container}   Returns the created button
 */
var createImageButton = function(w, h, img) {
  var buttonContainer = new createjs.Container();
  let source = MTLG.assets.getBitmap(img);
  if (!source.getBounds()) {
    if (!w) w = 20;
    if (!h) h = 20;
    source.setBounds(0, 0, w, h);
  }
  //if there is no image, the image was not found and instead it will be drawn a red roundRect.
  if (source.image === null) {
    var bounds = source.getBounds();
    console.error(l('rt_error:imageNotFound'), img);
    source = new createjs.Shape();
    source.setBounds(bounds.x, bounds.y, bounds.width, bounds.height);
    source.graphics.beginFill('red').drawRoundRect(0, 0, bounds.width, bounds.height, bounds.height / 10);
  }

  let scale = 1;
  let width = source.getBounds().width;
  let height = source.getBounds().height;
  //scale for the right width
  if (w) {
    scale = w / source.getBounds().width;
    buttonContainer.scaleX = buttonContainer.scaleY = scale;
    width = w;
    height = h || height * scale;
  }

  //determine, if the height is not greater than the given height
  if (h && source.getBounds().height * scale > h) {
    scale = h / source.getBounds().height;
    buttonContainer.scaleX = buttonContainer.scaleY = scale;
    height = h;
    width = w || source.getBounds().width * scale;
  }

  //positioning to the center
  if (source.getBounds().width * scale < w) {
    source.x = (w / 2 - source.getBounds().width * scale / 2) / scale;
  }
  if (source.getBounds().height * scale < h) {
    source.y = (h / 2 - source.getBounds().height * scale / 2) / scale;
  }

  buttonContainer.setBounds(0, 0, width, height);
  buttonContainer.addChild(source);
  return buttonContainer;
};


/*
 * makeContainerMovable - This function makes a container moveable by adding eventListener for mouseEvents
 *
 * @param  {type} container A createJS-Container- Object, which must have bounds defined
 * @param  {type} bounds    The x, y, width and height of the area in which the container is movable
 * @return {type}           description
 */
var makeContainerMovable = function(container, bounds, startCallback, endCallback) {
  var _offsetX;
  var _offsetY;
  var wasMoved;

  // Register Listener for handling the movement
  var pressupListener = container.on('pressup', handlePressup, null, false, {
    container: container
  });
  var pressmoveListener = container.on('pressmove', handlePressmove, null, false, {
    container: container
  });
  var mousedownListener = container.on('mousedown', handleMousedown, null, false, {
    container: container
  });
  // Handle for movement
  function handleMousedown(event, data) {
    // save the coordinates of the mousedown
    _offsetX = event.stageX - data.container.x;
    _offsetY = event.stageY - data.container.y;
  };

  function handlePressup(event, data) {
    data.container.mouseChildren = true;
    // Call endCallback only if it is defined and if the container was moved
    if (wasMoved && endCallback) endCallback();
    wasMoved = false;
    data.container.wasMoved = wasMoved;
  };

  function handlePressmove(event, data) {
    // Register movement
    if (_offsetX !== event.stageX - data.container.x || _offsetY !== event.stageY - data.container.y) wasMoved = true;
    else wasMoved = false;
    data.container.wasMoved = wasMoved;
    data.container.mouseChildren = false;
    if (startCallback) startCallback();
    var newX = event.stageX - _offsetX;
    var newY = event.stageY - _offsetY;

    // Move container to new pickPosition, if possible
    // Check for left hand side
    if (newX < 0) data.container.x = 0;
    //check for right hand side
    else if (newX + (data.container._bounds.width * MTLG.getScaleFactor()) > bounds.width) data.container.x = bounds.width - (data.container._bounds.width * MTLG.getScaleFactor());
    else data.container.x = newX;

    // Check for the top
    if (newY < 0) data.container.y = 0;
    // Check for the bottom
    else if (newY + (data.container._bounds.height * MTLG.getScaleFactor()) > bounds.height) data.container.y = bounds.height - (data.container._bounds.height * MTLG.getScaleFactor());
    else data.container.y = newY;
  };
};


/**
 * Global functions for the teacherView.
 * @namespace teacherView
 * @memberof MTLG.remoteteacher
 */
var teacherView = (function() {
  /* globalRoom saves the name of the global room and later all data for the players */
  var globalRoom = {
    name: null
  };
  var playerListContainer; //Container for metadata of the players
  var getPlayersFunction; //function for getting the players
  var _teacherToolbarStage; //Stage of the Toolbar in observer
  var _observerTest; //Test, if the observable response

  var _offsetX; // offset of the toolbar
  var _offsetY;
  var grid; //grid for seperate the differnt gamesessions in multiObserver


  /**
   * Initializes the global room with its name.
   *
   * @param  {string} name Name of the global room.
   * @memberof MTLG.remoteteacher.teacherView#
   */
  var init = function(name, getPlayers) {
    globalRoom.name = name;
    getPlayersFunction = getPlayers || defaultGetPlayers;
    //Get all metadata
    //set custom action for receiving metadata of the player rooms
    MTLG.distributedDisplays.actions.setCustomFunction('receiveMetadata', receiveMetadata);
    MTLG.distributedDisplays.communication.sendMessage(name, {
      action: "customAction",
      identifier: 'getMetadata',
      parameters: [MTLG.distributedDisplays.rooms.getClientId()]
    });
  }


  /**
   * drawTeacherToolbar - description
   *
   * @param  {string} playerId                    The id of the player which should be observed. Null for multiObserver
   * @param  {boolean} showOnlyBackAndMini = false True, if only the back and mini button of the toolbar should be shown.
   */
  function drawTeacherToolbar(playerId, showOnlyBackAndMini = false) {
    //If teacherToolbar does not exists, create it, otherwise only show it
    if (!document.getElementById('teacherToolbar')) {
      _teacherToolbarStage = new createjs.Stage(_addTeacherToolbarCanvas());
      createjs.Touch.enable(_teacherToolbarStage);
      _teacherToolbarStage.nextStage = MTLG.getStage();
      MTLG.menu.getMenuStage().nextStage = _teacherToolbarStage;
      var teacherToolbar = new toolbar(playerId, showOnlyBackAndMini);
      _teacherToolbarStage.addChild(teacherToolbar);
      createjs.Ticker.addEventListener("tick", _teacherToolbarStage);
      /*var rt = {
        nextRefresh: 1000,
        refreshDelta: 0,
        refreshFunction: function(delta) {

        },
        refreshInterval: 1000
      }
      createjs.Ticker.addEventListener("tick", function(event) {
        if (!event.paused) { // Ticker is not paused
          rt.refreshDelta += event.delta;
          rt.nextRefresh -= event.delta;
          if (rt.nextRefresh <= 0) {
            rt.nextRefresh += rt.refreshInterval;
            if (rt.nextRefresh < 0) rt.nextRefresh = 0;
            if (rt.refreshFunction) rt.refreshFunction(rt.refreshDelta);
            rt.refreshDelta = 0;
          }
        }
      });*/
    } else {
      _teacherToolbarStage.addChild(new toolbar(playerId, showOnlyBackAndMini));
    }
    resizeChildren(_teacherToolbarStage);
  }


  /**
   * toolbar - This function creates a toolbar as a createjs.Container for the remoteteacher to intevene.
   *
   * @param  {type} playerId            The id of the player which will be observed. Null for multiObserver
   * @param  {type} showOnlyBackAndMini True, if only the back and mini button of the toolbar should be shown.
   * @return {Container}                Returns the Toolbar Container
   */
  function toolbar(playerId, showOnlyBackAndMini) {
    this.container = new createjs.Container();
    this.container.name = "toolbar";
    var x = 5;
    var y = 5;
    var buttonHeight = 40;
    this.container.closeCallbacks = [];

    var containerMoveStartCallbackArray = [];
    var containerMoveEndCallbackArray = [];

    if (!showOnlyBackAndMini) {
      //add all tools, which are defined
      (function(container) {
        toolbarFunctions.forEach(function(value, key) {
          var [button, openCallback, closeCallback, allowFunction, startMove, endMove] = value(playerId, buttonHeight);
          if (closeCallback) container.closeCallbacks.push(closeCallback);
          if (startMove) containerMoveStartCallbackArray.push(startMove);
          if (endMove) containerMoveEndCallbackArray.push(endMove);
          button.on('pressup', function(event, data) {
            if (!data.container.wasMoved && openCallback) openCallback();
          }, null, false, {
            container: container
          })
          button.x = x;
          button.y = y;
          x += 5 + button.getBounds().width;
          container.addChild(button);
        });
      })(this.container);
    }
    var containerMoveStartCallback = function() {
      containerMoveStartCallbackArray.forEach(function(value, key) {
        value();
      });
    };
    var containerMoveEndCallback = function() {
      containerMoveEndCallbackArray.forEach(function(value, key) {
        value();
      });
    };
    makeContainerMovable(this.container, {
      x: 0,
      y: 0,
      width: _teacherToolbarStage.canvas.width,
      height: _teacherToolbarStage.canvas.height
    }, containerMoveStartCallback, containerMoveEndCallback);

    //create a back button to go back to the overview
    var backButton = createImageButton(undefined, buttonHeight / 2, 'img/remoteteacher/close.png'); //createButton(buttonHeight / 2, "Zurück", 'red', undefined, 55);
    backButton.x = x;
    backButton.y = y;
    backButton.name = "back";

    //create a back button to go back to the overview
    var minimizeButton = createImageButton(undefined, buttonHeight / 2, 'img/remoteteacher/minimize.png'); //createButton(buttonHeight / 2, "Mini", 'red', undefined, 55);
    minimizeButton.x = x;
    minimizeButton.y = y + buttonHeight / 2;
    minimizeButton.name = "minimize"

    x += 5 + minimizeButton.getBounds().width;

    this.container.addChild(backButton);
    this.container.addChild(minimizeButton);

    var minimizeListener = minimizeButton.on('pressup', function(event, data) {
      if (!data.container.wasMoved) toggleToolbar(event, data);
    }, null, false, {
      container: this.container
    });
    backButton.on('pressup', function(event, data) {
      if (!data.container.wasMoved) leaveObserverHandler(event, data);
    }, null, false, {
      callback: function() {
        MTLG.remoteteacher.teacherView.draw();
      },
      container: this.container
    });

    this.container.x = (_width * 50) - ((1 / 2) * (x - 5));
    this.container.y = 0;
    this.container.setBounds(this.container.x, this.container.y, x, buttonHeight + 10);

    //Container background
    var background = new createjs.Shape();
    background.x = 0;
    background.y = 0;
    background.graphics.beginFill('rgba(124,149,232,0.5)').drawRoundRect(0, 0, this.container.getBounds().width, this.container.getBounds().height, this.container.getBounds().height / 4);
    this.container.addChildAt(background, 0);

    return this.container;
  }

  /**
   * toggleToolbar - Hides or shows the toolbar.
   *
   * @param  {type} event description
   * @param  {object} data  Object with a container. {container: createjscontainer}s
   * @return {type}       description
   */
  function toggleToolbar(event, data) {
    // TODO:  auch back anzeigen
    for (var index in data.container.children) {
      if (data.container.children[index].name != "minimize" && data.container.children[index].name != "back") {
        data.container.children[index].visible = !data.container.children[index].visible;
      }
    }
  }

  /**
   * deleteToolbar - Deletes the toolbar.
   *
   */
  function deleteToolbar() {
    for (let index in _teacherToolbarStage.children) {
      if (_teacherToolbarStage.children[index].name === 'toolbar') {
        _teacherToolbarStage.children[index].closeCallbacks.forEach(function(value, key) {
          if (value) value();
        });
        _teacherToolbarStage.removeChild(_teacherToolbarStage.children[index]);
      }
    }
  }

  /**
   * _addTeacherToolbarCanvas -This function creates a canves element and adds it to the DOM.
   *
   * @return {canvas}  canvas element
   */
  function _addTeacherToolbarCanvas() {
    let newNode = MTLG.getStage().canvas.cloneNode(true);
    newNode.setAttribute('id', 'teacherToolbar');
    MTLG.getStage().canvas.parentNode.insertBefore(newNode, MTLG.getStage().canvas.nextSibling);
    return newNode;
  }


  /**
   * This function is for drawing the main overview for the remote teacher
   *
   * @param  {function} getPlayers Function to get all players, which should be displayed
   * @memberof MTLG.remoteteacher.teacherView#
   */
  function draw() {
    var container = new createjs.Container();

    //Checks, if the Stage has a background, if not, it adds a white background
    var background = MTLG.getBackgroundStage();
    if (background.children.length < 1) {
      MTLG.setBackgroundColor('#FFFFFF');
    }

    //set custom action for receiving metadata of the player rooms
    MTLG.distributedDisplays.actions.setCustomFunction('receiveMetadata', receiveMetadata);
    //register listener for an update of a sharedObject. This is for updating the metadata
    MTLG.distributedDisplays.actions.registerListenerForAction("updateSharedObject", customFunctionOnUpdate);

    //Container for existing playerRooms
    playerListContainer = new createjs.Container();
    playerListContainer.x = 10;
    playerListContainer.y = 10;
    playerListContainer.setBounds(playerListContainer.x, playerListContainer.y, _width * 100 - 210, _height - 2 * playerListContainer.y);

    //button to refresh the list of rooms
    var refreshButton = createButton(50, l('rt_refresh'), '#70ad47', "white", 100);
    refreshButton.y = _height * 50 - 100;
    refreshButton.x = (_width * 100 - 200);
    refreshButton.on('click', getAllPlayersToPlayerlist, null, false, {
      container: playerListContainer,
      getPlayers: getPlayersFunction
    });

    //button to refresh the list of rooms
    var multiObserverButton = createButton(50, l('rt_showAll'), '#70ad47', "white", 100);
    multiObserverButton.y = _height * 50 + 50;
    multiObserverButton.x = _width * 100 - 200;
    multiObserverButton.on('click', multiObserver, null, false);

    container.addChild(refreshButton);
    container.addChild(multiObserverButton);
    container.addChild(playerListContainer);

    //get all players and show them
    getAllPlayersToPlayerlist(null, {
      container: playerListContainer,
      getPlayers: getPlayersFunction
    });

    // Until a working global concept for resizing is found, circument lifeCycles's resizing
    setTimeout( () => {
      var stage = MTLG.getStage();
      stage.removeAllChildren();
      stage.addChild(container);
      resizeChildren(stage);
    });
  }

  /*
   * This function draws the default register page for the teacher. Here the teacher can join an exisiting room or create his own new room.
   *
   * @memberof MTLG.remoteteacher.teacherView#
   */
  function drawRoomHandler() {
    var stage = MTLG.getStage();
    var container = new createjs.Container();

    //remove all Objects of the current stage
    stage.removeAllChildren();
    rt.refreshInterval = Infinity;
    rt.nextRefresh = rt.refreshInterval;
    rt.refreshFunction = null;

    // Button for joining an existing room
    var joinExistingRoomsButton = createButton(100, l('rt_joinRoom'), "#70ad47", "white");
    joinExistingRoomsButton.x = 25 * _width - 1 / 2 * joinExistingRoomsButton.getBounds().width;
    joinExistingRoomsButton.y = 50 * _height - 1 / 2 * joinExistingRoomsButton.getBounds().height;

    joinExistingRoomsButton.on('click', function() {
      joinExistingRoom().catch(function(error) {
        if (error.reason) {
          swal({
            type: 'error',
            title: l('rt_title:errorCreatingRoom'),
            text: l('rt_error:creatingRoom') + error.reason
          });
        } else {
          console.error(l('rt_error:unknown'));
        }
      });
    });

    // Button for creating a new room
    var createRoomButton = createButton(100, l('rt_createRoom'), "#70ad47", "white");
    createRoomButton.x = 75 * _width - 1 / 2 * createRoomButton.getBounds().width;
    createRoomButton.y = 50 * _height - 1 / 2 * createRoomButton.getBounds().height;

    createRoomButton.on('click', function() {
      createRoom().then(function(result) {
        if (result && result.success) {
          init(result.name);
        } else {
          return Promise.reject(result);
        }
      }).then(draw).catch(function(error) {
        if (error.dismiss) {
          //do nothing
        } else {
          swal({
            type: 'error',
            title: l('rt_title:errorCreatingRoom'),
            text: l('rt_error:creatingRoom') + error.reason
          });
          console.error(error)
        }
      });
    });

    container.addChild(joinExistingRoomsButton);
    container.addChild(createRoomButton);

    stage.addChild(container);
    resizeChildren(stage);
    stage.update();
  }


  /**
   * joinExistingRoom - This function shows all remote teacher rooms. The teacher can click on a room to join it.
   *
   * @return {Promise}  The promise resolves, when all rooms are shown.
   */
  var joinExistingRoom = function() {
    var stage = MTLG.getStage();

    var background = MTLG.getBackgroundStage();
    if (background.children.length < 1) {
      MTLG.setBackgroundColor('#FFFFFF');
    }

    //get all rooms
    return new Promise(function(resolve, reject) {
      MTLG.distributedDisplays.rooms.getAllRooms(function(result) {
        //remove all Objects of the current stage
        stage.removeAllChildren();

        if (result && !(result.success === false)) {
          var container = new createjs.Container();
          stage.addChild(container);
          resizeChildren(stage);

          var x, y = 0;
          var size = 50;
          var padding = 5;
          var noRoom = true;
          var count = Object.keys(result).length;
          var index = 0;
          var numberHorizontal = parseInt(Math.sqrt(count - 1) + 1);
          var numberVertical = Math.ceil(count / numberHorizontal);
          var lastWidth = 0;
          var oriX = _width * (1 / (1 + numberHorizontal)) * 100;
          var oriY = _height * (1 / (1 + numberVertical)) * 100;

          for (let roomName in result) {
            //filter rooms: only rooms with the teacherprefix should be displayed,
            // because a player should only enter the teacher room an not other player rooms
            if (roomName.startsWith(teacherRoomPrefix)) {
              var roomWithoutPrefix = roomName.substring(teacherRoomPrefix.length);

              //calculate coordinates for the next button.
              if ((index % numberHorizontal) != 0) x = (index % numberHorizontal + 1) * oriX;
              else {
                x = oriX;
                y = oriY * (parseInt(index / numberVertical) + 1);
              }

              //show room name without prefix
              var button = createButton(size, roomWithoutPrefix);
              button.x = x;
              button.y = y;
              button.regX = button.getBounds().width / 2;
              button.regY = button.getBounds().height / 2;
              lastWidth = button.getBounds().width;

              (function(name) {
                button.on('click', joinRoomHandler, null, true, {
                  name: name,
                  callback: function(result) {
                    init(result.name);
                    draw();
                  }
                });
              })(roomName);

              container.addChild(button);
              stage.update();
              noRoom = false;
            }
            index++;
          }

          if (noRoom) {
            var textNoRoom = new createjs.Text(l('rt_noRoomsAvailable'), '12px Arial', 'black');
            textNoRoom.x = _width * 50 - textNoRoom.getBounds().width / 2;
            textNoRoom.y = _height * 50 - textNoRoom.getBounds().height / 2;
            stage.addChild(textNoRoom);
            resizeChildren(stage);
            stage.update();
          }
          resolve();
        } else {
          reject(result);
        }
      });
    });
  }

  /**
   * makePlayerMetadataContainer - This function displays all Metadata of one player
   *
   * @param  {type} playerId The id of the player, whose metadata should be shown
   * @param  {type} bounds   Bounds of the outer container
   * @return {type}          Container with metadata
   */
  var makePlayerMetadataContainer = function(playerId, width) {
    // general information for the container
    var containerWidth = width
    var font = '12px Arial';
    var color = 'black';
    var padding = 5;
    var containerWidthInner = containerWidth - 2 * padding;

    var container = new createjs.Container();

    //title should be on the top left, but with padding
    // The title of the box (the clientId)
    var title = new createjs.Text(playerId, 'bold 15px Arial', '#f00');
    title.x = padding;
    title.y = padding;
    container.addChild(title);
    var x = padding;
    var y = title.getBounds().height + padding + 10; //10 pixel extra buffer
    //add all metadata
    for (var key in globalRoom[playerId].metadata) {
      if (key === "sharedId") continue;
      var metaTitle = new createjs.Text(key + ': ', 'bold ' + font, color);
      metaTitle.x = x;
      metaTitle.y = y;

      var metaInfo = new createjs.Text(globalRoom[playerId].metadata[key], font, color);
      metaInfo.x = x + metaTitle.getBounds().width;
      metaInfo.y = y;
      metaInfo.lineWidth = containerWidthInner - metaTitle.getBounds().width;

      //add to container
      container.addChild(metaTitle);
      container.addChild(metaInfo);

      //increase y-coordinate
      y += metaInfo.getBounds().height + padding;
    }

    container.setBounds(0, 0, containerWidth, y + padding);
    //Container background
    var background = new createjs.Shape();
    background.x = 0;
    background.y = 0;
    background.graphics.beginFill('rgba(124,149,232,0.5)').drawRoundRect(0, 0, container.getBounds().width, container.getBounds().height, 10);
    container.addChildAt(background, 0);

    //add Eventlistener to show this playerRoom
    container.on('click', function() {
      rt.refreshInterval = Infinity;
      rt.nextRefresh = rt.refreshInterval;
      rt.refreshFunction = null;
      drawObserver(playerId);
    }, null, true);

    return container;
  }


  /**
   * defaultGetPlayers - Default function for getting the players. In this case the players are the clients of all joined rooms of the teacher
   *
   * @return {Promise}  Returns a Promise with alle clients in an array
   */
  var defaultGetPlayers = function() {
    return new Promise(function(resolve, reject) {
      return MTLG.distributedDisplays.rooms.getJoinedRooms(function(result) {
        if (result && !(result.success === false)) {
          //save all clients in the array for resolving
          var clients = [];
          for (let roomName in result) {
            for (let clientId in result[roomName].sockets) {
              if (clientId === MTLG.distributedDisplays.rooms.getClientId()) continue;
              clients.push(clientId);
            }
          }
          resolve(clients);
        } else {
          reject(result);
        }
      });
    });
  };

  /**
   * getAllPlayersToPlayerlist - This function gets all players and shows them on the display.
   *
   * @param  {event} event description
   * @param  {Object} data  Object with the container, in which the players should be shown and the function to get the players
   * <pre><code> {container: containerForPlayer, getPlayers: getPlayersFunction}</code></pre>
   * @return {Promise}       description
   */
  var getAllPlayersToPlayerlist = function(event, data) {
    if(!data.container) return Promise.resolve('no container yet');

    var playerList = data.container;
    var getPlayers = data.getPlayers || defaultGetPlayers;
    var stage = MTLG.getStage();

    return new Promise(function(resolve, reject) {
      return getPlayers().then(function(result) {
        if (result && !(result.success === false)) {
          playerList.removeAllChildren();
          var x, y = 0;
          var padding = 5;
          for (let index in result) {
            var clientId = result[index];
            //if it is the own clientId continue
            if (clientId === MTLG.distributedDisplays.rooms.getClientId()) continue;
            (function(client) {
              if (!globalRoom[client]) globalRoom[client] = {}; //if the client is not define, define it
              var container = makePlayerMetadataContainer(client, playerList.getBounds().width / 2 - 2 * padding); // playerList is divided into two parts
              globalRoom[client].container = container;

              if ((index % 2) != 0) container.x = playerList.getBounds().width / 2 + padding;
              else container.x = 0;
              container.y = y;

              //increase the y-coordinate for the next container
              if ((index % 2) != 0) y += (container.getBounds().height /** MTLG.getScaleFactor()*/ ) + 5;
              playerList.addChild(container);
            }(clientId));
            stage.update();
          }
          resolve();
        } else {
          reject();
        }
      }, function(err) {
        var errText = new createjs.Text(l('error:getPlayers'), '12px Arial', 'red');
        errText.x = playerList.getBounds().width / 2;
        errText.y = playerList.getBounds().height / 2;
        playerList.addChild(errText);
        stage.update();
        console.error("Remote Teacher: " + err);
      });
    });
  };


  /**
   * multiObserver - This function is for drawing the overview over the screen of all players
   *
   */
  var multiObserver = function() {
    var stage = MTLG.getStage();
    //set custom actio for receiving metadata of the player rooms
    MTLG.distributedDisplays.actions.removeCustomFunction('receiveMetadata');
    MTLG.distributedDisplays.actions.deregisterListenerForAction("updateSharedObject", customFunctionOnUpdate);
    stage.removeAllChildren();

    //send a message to all players, so that they answer with their sharedArea
    getPlayersFunction().then(function(result) {
      if (result && !(result.success === false)) {
        MTLG.distributedDisplays.actions.setCustomFunction('receiveSharedArea', customFunctionReceiveSharedArea);
        for (let index in result) {
          var clientId = result[index];
          MTLG.distributedDisplays.communication.sendMessage(clientId, {
            action: "customAction",
            identifier: 'getSharedArea',
            parameters: [MTLG.distributedDisplays.rooms.getClientId(), config.globalScale]
          });
        }
      }
    }, function(error) {
      var errText = new createjs.Text(l('rt_error:getPlayers'), '12px Arial', 'red');
      errText.x = _width / 2;
      errText.y = _height / 2;
      stage.addChild(errText);
      console.error("Remote Teacher: " + err);
    });
    drawTeacherToolbar(null, true);
  }


  /**
   * drawObserver - This function draws the view of observing one gamesession.
   *
   * @param  {number} clientId The id of the client, who will be observed
   */
  var drawObserver = function(clientId) {
    var stage = MTLG.getStage();
    stage.removeAllChildren();
    MTLG.distributedDisplays.actions.setCustomFunction('receiveSharedArea', customFunctionReceiveSharedArea);
    // Send a message to the client, so that he can answer with the shared Area
    MTLG.distributedDisplays.communication.sendMessage(clientId, {
      action: "customAction",
      identifier: 'getSharedArea',
      parameters: [MTLG.distributedDisplays.rooms.getClientId(), 1]
    });
    drawTeacherToolbar(clientId);
    //Test, if the client has responded
    _observerTest = setTimeout(function() {
      if (!globalRoom[clientId].sharedArea || globalRoom[clientId].sharedArea.length < 1) {
        swal({
          type: 'error',
          title: l('rt_title:noRespondOfPlayer'),
          text: l('rt_error:noRespondOfPlayer')
        }).then(function(result) {
          //Go back to main screen, because here is nothing to see.
          leaveObserverHandler(undefined, {
            callback: function() {
              MTLG.remoteteacher.teacherView.draw();
            }
          });
        });
      }
    }, 5000);
  };


  /**
   * leaveObserverHandler - This function leaves the observer mode and goes back to the main overview.
   *
   * @param  {event} event description
   * @param  {object} data  object with a callback. {callback: () => ()}
   * @return {type}       description
   */
  var leaveObserverHandler = function(event, data) {
    //remove the custom function, so that the teacher does not receive any shared area anymore
    MTLG.distributedDisplays.actions.removeCustomFunction('receiveSharedArea');
    // delete the shared areas
    for (var client in globalRoom) {
      if (globalRoom[client].sharedArea) {
        for (let index in globalRoom[client].sharedArea) {
          globalRoom[client].sharedArea[index].delete();
        }
        globalRoom[client].sharedArea = null;
      }
    }
    //clear the timeout of the observer Test, because otherwise the test would fail and the user would get an error
    if (_observerTest) clearTimeout(_observerTest);
    _teacherToolbarStage.visible = false;
    //delete the toolbar, because the toolbar is specific for one gamesession (clientId) and in most cases the next time another client will be observed
    deleteToolbar();
    data.callback();
  };

  /****************************************** CUSTOM FUNCTIONS ********************************************/

  /**
   * receiveMetadata - This function handles the message "receiveSharedArea" and shows the received metadata on the screen.
   *
   * @param  {number} from     The id of the client, who sent the message
   * @param  {Object} metadata The sharedObject of the metadata
   */
  var receiveMetadata = function(from, metadata) {
    //set the update function
    MTLG.distributedDisplays.actions.setCustomFunction('updateMetadata', updateMetadata);
    //add the shared metadata object
    MTLG.distributedDisplays.actions.addSharedObjects(metadata);
    // Get the sharedId from the associative list
    var sharedId;
    for (var i in metadata) {
      if (metadata[i].sharedId) {
        sharedId = metadata[i].sharedId;
        break;
      }
    }

    var metadataSharedObj = MTLG.distributedDisplays.SharedObject.getSharedObject(sharedId);

    //save the metadataObject for later use
    if (globalRoom[from]) {
      globalRoom[from].metadata = metadataSharedObj.createJsObject;
    } else {
      globalRoom[from] = {
        metadata: metadataSharedObj.createJsObject
      };
    }

    //refresh the playerlist with their metadata
    getAllPlayersToPlayerlist(null, {
      container: playerListContainer,
      getPlayers: getPlayersFunction
    });
  };


  /**
   * updateMetadata - This function handles the message "updateMetadata". It saves the new metadata
   *
   * @param  {type} from        description
   * @param  {type} metadata    description
   * @param  {type} oldSharedId description
   * @return {type}             description
   */
  var updateMetadata = function(from, metadata, oldSharedId) {
    //delete the old one
    var oldMetadataSharedObj = MTLG.distributedDisplays.SharedObject.getSharedObject(oldSharedId);
    oldMetadataSharedObj.delete();

    //add the new one
    MTLG.distributedDisplays.actions.addSharedObjects(metadata);
    var sharedId;
    for (var i in metadata) {
      if (metadata[i].sharedId) {
        sharedId = metadata[i].sharedId;
        break;
      }
    }
    var metadataSharedObj = MTLG.distributedDisplays.SharedObject.getSharedObject(sharedId);
    globalRoom[from].metadata = metadataSharedObj.createJsObject;
  };


  /**
   * customFunctionReceiveSharedArea - This function handles the message "receiveSharedArea".
   * It displays the received areas on the scree. The third parameter detemines how to scale the areas.
   *
   * @param  {string} from            The id of the sender of the message
   * @param  {array} areas           Array of Sendables
   * @param  {number} globalScale = 1 This scales indicates how to scale the given container
   */
  var customFunctionReceiveSharedArea = function(from, areas, globalScale = 1) {
    var stage = MTLG.getStage();
    var countExistingSharedAreas = 0;
    for (var client in globalRoom) {
      //the client, which send this message should not be counted
      if (client !== from && globalRoom[client].sharedArea) countExistingSharedAreas++;
    }

    if (areas && areas.length > 0) {
      for (let index in areas) {
        var area = areas[index];
        // Get the sharedId of the area from the associative list
        var sharedId;
        for (var i in area) {
          if (area[i].sharedId) {
            sharedId = area[i].sharedId;
            break;
          }
        }

        //With addSharedObjects the objects are automatically added to the stage and synchronized
        try {
          MTLG.distributedDisplays.actions.addSharedObjects(area);
        } catch (err) {
          //remove Tick because the tick forces the stage to update, but this produces the error
          createjs.Ticker.removeEventListener("tick", stage);
          //save the sharedArea, to savely delete it.
          var sharedObj = MTLG.distributedDisplays.SharedObject.getSharedObject(sharedId);
          //save the sharedArea
          if (!globalRoom[from].sharedArea) globalRoom[from].sharedArea = [];
          globalRoom[from].sharedArea.push(sharedObj);
          leaveObserverHandler(undefined, {
            callback: function() {
              MTLG.remoteteacher.teacherView.draw();
              swal({
                type: 'error',
                title: l('rt_title:DD'),
                text: l('rt_errorText:addSharedObjects') + JSON.stringify(err)
              })
              createjs.Ticker.addEventListener("tick", stage);
            }
          });
          console.error(l('rt_error:addSharedObjects'), err);
          return;

        }
        var sharedObj = MTLG.distributedDisplays.SharedObject.getSharedObject(sharedId);
        //Because the window of the sender could have other bounds, the scale has to be calculated, otherwise the original scale of the sender is used.
        var scaleX = stage.canvas.width / MTLG.getOptions().width * globalScale;
        var scaleY = stage.canvas.height / MTLG.getOptions().height * globalScale;

        // calculate new x and y coordinates, because the scale is based on the center and not the zero-point
        var newX = (scaleX * sharedObj.createJsObject.x / sharedObj.createJsObject.scaleX) + ((countExistingSharedAreas % Math.pow(globalScale, -1)) * scaleX * MTLG.getOptions().width);
        var newY = (scaleY * sharedObj.createJsObject.y / sharedObj.createJsObject.scaleY) + (parseInt(countExistingSharedAreas / Math.pow(globalScale, -1)) * scaleY * MTLG.getOptions().height);

        sharedObj.createJsObject.scaleX = scaleX;
        sharedObj.createJsObject.scaleY = scaleY;
        sharedObj.createJsObject.x = newX;
        sharedObj.createJsObject.y = newY;

        //save the sharedArea
        if (!globalRoom[from].sharedArea) globalRoom[from].sharedArea = [];
        globalRoom[from].sharedArea.push(sharedObj);
      }

      //if the global scale is not 1, a grid should be displayed, so that single screens are seperated
      if (globalScale !== 1) {
        if (!grid || grid.children.length < 1 || grid.parent === null) {
          grid = new createjs.Container();

          //determine how many stripse
          var numberStripse = Math.pow(globalScale, -1) - 1;
          for (var i = 1; i <= numberStripse; i++) {
            var horizontal = new createjs.Shape();
            var x = i * globalScale * stage.canvas.width;
            var y = stage.canvas.height;
            horizontal.graphics.setStrokeStyle(2).beginStroke("black").moveTo(x, 0).lineTo(x, y);

            var vertical = new createjs.Shape();
            x = stage.canvas.width;
            y = i * globalScale * MTLG.getStage().canvas.height;
            vertical.graphics.setStrokeStyle(2).beginStroke("black").moveTo(0, y).lineTo(x, y);

            grid.addChild(horizontal, vertical);
          }
          stage.addChild(grid);
        } else {
          //here the grid is first removed and directly added, because it has to be foregrounded.
          stage.removeChild(grid);
          stage.addChild(grid);
        }
      }
    } else {
      var errText = new createjs.Text(l('rt_warn:nothingShared'), '12px Arial', 'red');
      errText.x = (MTLG.getStage().canvas.width / 2 - errText.getBounds().width / 2) * globalScale;
      errText.y = (MTLG.getStage().canvas.height / 2 - errText.getBounds().height / 2) * globalScale;

      stage.addChild(errText);
      stage.update();
      console.warn(l('rt_warn:nothingShared'));
    }
    globalRoom[from].globalScale = globalScale;
  };


  /**
   * customFunctionOnUpdate - This function is called, when any sharedObject was updated. This function is only to update the display, if the metadata has changed.
   *
   * @param  {object} action This parameter is an Object, which looks like: <pre><code> {
     action: "updateSharedObject",
      sharedId: sharedId,
      propertyPath: propertyPath,
      oldValue: oldValue,
      newValue: newVal,
      additionalInfo: additionalInfo,
      from: from
    };</code></pre>
    */
  var customFunctionOnUpdate = function(action) {
    if (globalRoom[action.from] && action.sharedId === globalRoom[action.from].metadata.sharedId) {
      //call this funtion again to rerender the complete metadata of all Users
      // it would be sufficient to update only the container of the current user
      // but then you have to struggle with the postioning, because the container could be bigger than before
      // so the following containers had to reposition again
      getAllPlayersToPlayerlist(null, {
        container: playerListContainer,
        getPlayers: getPlayersFunction
      })
      return;
    }
  };


  /**
   * Gets the Stage of the toolbar
   *
   * @return {Stage}  Returns the stage of the toolbar.
   * @memberof MTLG.remoteteacher.teacherView#
   */
  var getToolbarStage = function() {
    return _teacherToolbarStage;
  }

  return {
    draw: draw, //draws the main screen of the remote teacher
    init: init, //initializes with name and listener
    drawRoomHandler: drawRoomHandler, //for standard role allocation
    getToolbarStage: getToolbarStage
  }
})();



/**
 * @namespace playerView
 * @memberof MTLG.remoteteacher
 */
var playerView = (function() {

  var localRoom = {
    name: null, //client or room id of the remote teacher
    sharedArea: null, //array of all areas, that will be shared
    metadata: {
      metadata: false //false to show, that are not metadata at the moment
    }
  };


  /**
   * init - This function initializes the player view.
   *
   * @param  {type} name             ClientId or Room of the remoteteacher
   * @param  {type} metadata = false If already known, the metadata can be set.
   * @memberof MTLG.remoteteacher.playerView#
   */
  var init = function(name, metadata = false) {
    localRoom.name = name;
    sendMetadata(name, metadata);
    //set custom action for sending metadata
    MTLG.distributedDisplays.actions.setCustomFunction('getMetadata', sendMetadata);
    MTLG.distributedDisplays.actions.setCustomFunction('getSharedArea', sendSharedArea);

    //initialize active intervention
    toolbarFunctions.forEach(function(value, key) {
      var [button, openCallback, closeCallback, allowFunction] = value();
      allowFunction();
    });
  }


  /**
   * sendMetadata - This function sends the given metadata to the given client or room.
   *
   * @param  {type} roomName         ClientId or room of the receivers
   * @param  {type} metadata = false metadata object, which should be send
   */
  var sendMetadata = function(roomName, metadata = false) {
    var sharedId = false;

    //if no object already exists, create it, otherwise take the existing sharedId
    if (!localRoom.metadata) {
      localRoom.metadata = metadata || {
        metadata: false
      };
    } else {
      sharedId = localRoom.metadata.sharedId;
    }

    //Create a new SharedObject possibly with the existing sharedId
    var sharedMetadata = new MTLG.distributedDisplays.SharedObject(localRoom.metadata, false, true, sharedId);
    sharedMetadata.syncProperties = {};
    for (var prop in localRoom.metadata) {
      sharedMetadata.syncProperties[prop] = {
        send: true,
        receive: true
      }
    }
    sharedMetadata.init();
    var send = sharedMetadata.toSendables();
    MTLG.distributedDisplays.communication.sendMessage(roomName, {
      action: "customAction",
      identifier: 'receiveMetadata',
      addServerListener: send,
      parameters: [MTLG.distributedDisplays.rooms.getClientId(), send]
    });
  }


  /**
   * drawRoomSelection - This function draws a selection of all existing remote-teacher rooms.
   * The player can choose which room he or she wants to join.
   *
   * @function draw
   * @param  {type} nextLevel = 1 The gameLevel of the level which is next
   * @return {type}               description
   * @memberof MTLG.remoteteacher.playerView#
   */
  var drawRoomSelection = function(gameState = {nextLevel: 1}) {
    var stage = MTLG.getStage();

    //if no background is set, set a white background
    var background = MTLG.getBackgroundStage();
    if (background.children.length < 1) {
      MTLG.setBackgroundColor('#FFFFFF');
    }

    //for refreshing the list of available rooms every second
    rt.refreshInterval = 1000;
    rt.nextRefresh = rt.refreshInterval;
    rt.refreshFunction = function() {
      drawRoomSelection(gameState);
    };

    //get all rooms
    MTLG.distributedDisplays.rooms.getAllRooms(function(result) {
      //remove all Objects of the current stage
      stage.removeAllChildren();

      if (result && !(result.success === false)) {
        var container = new createjs.Container();
        stage.addChild(container);
        resizeChildren(stage);

        var x, y = 0;
        var size = 50;
        var padding = 5;
        var noRoom = true;
        var count = Object.keys(result).length; // count how many rooms are available for positioning the single buttons dynamically
        var index = 0;
        var numberHorizontal = parseInt(Math.sqrt(count - 1) + 1); //calculate how many buttons should be shown horizontal next to each other
        var numberVertical = Math.ceil(count / numberHorizontal); //calculate how many buttons should be shown vertical next to each other
        var lastWidth = 0;
        var oriX = _width * (1 / (1 + numberHorizontal)) * 100; //the first x-coordinate
        var oriY = _height * (1 / (1 + numberVertical)) * 100; //the first y-coordinate

        for (let roomName in result) {
          //filter rooms: only rooms with the teacherprefix should be displayed,
          // because a player should only enter the teacher room an not other player rooms
          if (roomName.startsWith(teacherRoomPrefix)) {
            var roomWithoutPrefix = roomName.substring(teacherRoomPrefix.length);

            //calculate coordinates for the next button.
            if ((index % numberHorizontal) != 0) x = (index % numberHorizontal + 1) * oriX; //set the next x step
            else {
              x = oriX; // set to the first x value
              y = oriY * (parseInt(index / numberVertical) + 1); // set the next y step
            }

            //show room name without prefix
            var button = createButton(size, roomWithoutPrefix);
            button.x = x;
            button.y = y;
            button.regX = button.getBounds().width / 2;
            button.regY = button.getBounds().height / 2;
            lastWidth = button.getBounds().width;

            (function(name) {
              button.on('click', joinRoomHandler, null, true, {
                name: name,
                callback: function(result) {
                  rt.refreshFunction = null;
                  rt.refreshInterval = Infinity;
                  rt.nextRefresh = rt.refreshInterval;

                  init(name);
                  //game starts after joining the room
                  if(!gameState.nextLevel) gameState.nextLevel = 1;
                  MTLG.lc.levelFinished(gameState);
                }
              });
            })(roomName);

            container.addChild(button);
            stage.update();
            noRoom = false;
          }
          index++;
        }

        //If no room is available, a little note is shown to the user
        if (noRoom) {
          var textNoRoom = new createjs.Text(l('rt_noRoomsAvailable'), '12px Arial', 'black');
          textNoRoom.x = _width * 50 - textNoRoom.getBounds().width / 2;
          textNoRoom.y = _height * 50 - textNoRoom.getBounds().height / 2;
          stage.addChild(textNoRoom);
          resizeChildren(stage);
          stage.update();
        }
      } else {
        var errText = new createjs.Text(l('rt_error:getAllRooms'), '12px Arial', 'red');
        errText.x = _width * 50 - textNoRoom.getBounds().width / 2;
        errText.y = _height * 50 - textNoRoom.getBounds().height / 2;
        resizeChildren(stage);
        stage.addChild(errText);
        stage.update();
        console.error('Remote teacher:', result);
      }
    });

  };


  /**
   * setMetadata - This is the public method to give the metadate, so that they can be send to the remote teacher.
   *
   * @param  {type} metadata object with the metadata
   * @return {type}          description
   * @memberof MTLG.remoteteacher.playerView#
   */
  var setMetadata = function(metadata) {
    //if there is no sharedId, the metadata should be send for the first time.
    if (!localRoom.metadata.sharedId) {
      sendMetadata(localRoom.name, metadata);
    } else {
      var oldSharedId = localRoom.metadata.sharedId;
      var sharedMetadata = new MTLG.distributedDisplays.SharedObject(metadata, false, true);

      //set the syncProperties, it is no CreateJS object so the single properties has to be set manually
      sharedMetadata.syncProperties = {};
      for (var prop in metadata) {
        sharedMetadata.syncProperties[prop] = {
          send: true,
          receive: true
        }
      }
      localRoom.metadata = metadata;
      sharedMetadata.init();
      var send = sharedMetadata.toSendables();
      MTLG.distributedDisplays.communication.sendMessage(localRoom.name, {
        action: "customAction",
        identifier: 'updateMetadata',
        addServerListener: send,
        parameters: [MTLG.distributedDisplays.rooms.getClientId(), send, oldSharedId]
      });
    }
  };


  /**
   * areaForSharing - This is a public method the set the areas, which should be shared.
   * The areas are saved for later use. They will be send, if the remoteteacher asks for them.
   *
   * @function forSharing
   * @param  {type} areas          array of containers, which should be shared.
   * @param  {type} add = false    If true the new array is added to the existing one.
   *  Otherwise it is replaced. Automatically it will be send to the receiver as an update.
   * @param  {type} update = false If true the areas are send to the receiver as an update.
   * @return {type}                description
   * @memberof MTLG.remoteteacher.playerView#
   */
  var areaForSharing = function(areas, add = false, update = false) {
    if (localRoom.sharedArea === null || add) {
      if (!add) localRoom.sharedArea = [];
      var send = [];

      //if the area is the stage, the children will be shared
      if (areas === MTLG.getStage()) {
        areas = areas.children;
      }

      //the syncProperties for the single areas are set
      for (let index in areas) {
        var area = areas[index];
        var sharedObj = new MTLG.distributedDisplays.SharedObject(area, false, true);
        sharedObj.syncProperties.scaleX = {
          send: true,
          receive: false
        };
        sharedObj.syncProperties.scaleY = {
          send: true,
          receive: false
        };
        sharedObj.syncProperties.x = {
          send: true,
          receive: false
        };
        sharedObj.syncProperties.y = {
          send: true,
          receive: false
        };
        sharedObj.init();

        localRoom.sharedArea.push(sharedObj);
        send.push(sharedObj.toSendables());
      }

      if ((update || add) && localRoom.name) {
        MTLG.distributedDisplays.communication.sendMessage(localRoom.name, {
          action: "customAction",
          identifier: 'receiveSharedArea',
          addServerListener: send,
          parameters: [MTLG.distributedDisplays.rooms.getClientId(), send, localRoom.globalScale]
        });
      }

    } else {

      for (let index in localRoom.sharedArea) {
        var area = localRoom.sharedArea[index];

        area.delete(true);
        MTLG.distributedDisplays.communication.sendSharedObjectDeletion(localRoom.name, area.sharedId)
      }

      localRoom.sharedArea = null;
      setTimeout(function() {
        areaForSharing(areas, false, true);
      }, 500);
    }
  };


  /**
   * sendSharedArea - This function sends the already defined sharedAreas to the sender, who asked for the sharedArea (message 'getSharedArea'). The globalScale is the scale for scaling on remote teacher side.
   *
   * @param  {type} from        The sender, who asked for the data.
   * @param  {type} globalScale Scale for receiver
   */
  var sendSharedArea = function(from, globalScale) {
    var send = false;
    localRoom.globalScale = globalScale;
    if (localRoom.sharedArea && localRoom.sharedArea !== null) {
      send = [];
      for (let area in localRoom.sharedArea) {
        send.push(localRoom.sharedArea[area].toSendables());
      }
    }
    MTLG.distributedDisplays.communication.sendMessage(from, {
      action: "customAction",
      identifier: 'receiveSharedArea',
      addServerListener: send,
      parameters: [MTLG.distributedDisplays.rooms.getClientId(), send, globalScale]
    });
  };

  return {
    draw: drawRoomSelection, // room selection for standard role allocation
    init: init, // initializes the player view with name and listener
    forSharing: areaForSharing, //method to define the ares to be shared
    setMetadata: setMetadata // method to define the metadata to be shared
  }
})();

/*********************** refresh Function Handler *****************************************/
var rt = {
  nextRefresh: Infinity,
  refreshDelta: 0,
  refreshFunction: null,
  refreshInterval: Infinity
}

// Polling for refresh
var handleTick = function(event) {
  if (!event.paused) { // Ticker is not paused
    rt.refreshDelta += event.delta;
    rt.nextRefresh -= event.delta;
    if (rt.nextRefresh <= 0) {
      rt.nextRefresh += rt.refreshInterval;
      if (rt.nextRefresh < 0) rt.nextRefresh = 0;
      if (rt.refreshFunction) rt.refreshFunction(rt.refreshDelta);
      rt.refreshDelta = 0;
    }
  }
};
createjs.Ticker.addEventListener("tick", handleTick);

/****************************************** toolbarFunctions *******************************************************/

/*
 * toolbarFunctionFeedback - self-invoking expression.
 *
 * @return {Object}            Returns an object with all global functions of the Feedback
 * <pre><code>{
 * getToolbarElement: addFeedbackToToolbar
 * }</code></pre>
 */
var toolbarFunctionFeedback = (function() {


  /**
   * addFeedbackToToolbar - This function creates a button for the remoteteacher toolbar and a function for activating the receiving of the feedback on player side.
   *
   * @param  {string} playerId     The id of the player, who should get the feedback
   * @param  {number} buttonHeight number, how high the button should be.
   * @return {destructor}              [button, null, allowFunction]
   */
  var addFeedbackToToolbar = function(playerId, buttonHeight) {
    if (!playerId) {
      return [null, null, null, feedbackController.player.allowFeedback];
    }
    var button = createButton(buttonHeight, l('rt_feedback'), 'green');
    openCallback = function() {
      feedbackController.teacher.giveFeedback(playerId);
    };
    return [button, openCallback, null, feedbackController.player.allowFeedback];
  }


  /**
   * feedbackView - The view for the feedback. This creates a dialog window to collect information for the feedback.
   *
   * @return {Object}
   * <pre><code>{
   *  draw: drawCreationModal
   * }</code></pre>
   */
  var feedbackView = function() {


    /**
     * drawCreationModal - This function draws the dialog for getting the information for the feedback.
     *
     * @return {Promise}  description
     */
    var drawCreationModal = function() {
      try { //try because swal could be the wrong version without mixin or queue.
        return swal.mixin({
          confirmButtonText: l('rt_next'),
          cancelButtonText: l('rt_cancel'),
          showCancelButton: true,
          progressSteps: ['1', '2', '3', '4']
        }).queue([{
          titleText: 'Target',
          html: '<div style="display: block;">Define Target</div>' +
            '<input id="target-rotation" class="swal2-input" placeholder="' + l('rt_rotation') + '" style="width: 90%;" /><span class="tooltip"><img src="img/fbm/windowRequestDark.png" alt="?" width="24" height="24"><span class="tooltiptext">' + l('rt_help_rotation') + '</span></span>' +
            '<label for="timing_movable" class="swal2-checkbox" style="display: flex;"><input type="hidden" /><input type="checkbox" id="timing_movable" /><span>' + l('rt_movable') + '</span></label>' +
            '<label for="timing_movableMini" class="swal2-checkbox" style="display: flex;"><input type="checkbox" id="timing_movableMini" /><span>' + l('rt_movableMini') + '</span></label>' +
            '<label for="timing_rotateOnMove" class="swal2-checkbox" style="display: flex;"><input type="checkbox" id="timing_rotateOnMove" /><span>' + l('rt_rotateOnMove') + '</span></label>' +
            '<label for="timing_minimizable" class="swal2-checkbox" style="display: flex;"><input type="checkbox" id="timing_minimizable" /><span>' + l('rt_minimizable') + '</span></label>' +
            '<label for="timing_closeable" class="swal2-checkbox" style="display: flex;"><input type="checkbox" id="timing_closeable" /><span>' + l('rt_closeable') + '</span></label>',
          preConfirm: function() {
            return {
              rotation: document.getElementById('target-rotation').value,
              closeable: document.getElementById('timing_closeable').checked,
              minimizable: document.getElementById('timing_minimizable').checked,
              movable: document.getElementById('timing_movable').checked,
              movableMini: document.getElementById('timing_movableMini').checked,
              rotateOnStart: null,
              rotateOnMove: document.getElementById('timing_rotateOnMove').checked,
            }
          },
          showLoaderOnConfirm: true
        }, {
          titleText: 'Content',
          html: '<div style="display: block;">Define Content</div>' +
            '<input id="content_text" class="swal2-input" placeholder="' + l('rt_content') + '" style="width: 90%;" /><span class="tooltip"><img src="img/fbm/windowRequestDark.png" alt="?" width="24" height="24"><span class="tooltiptext">' + l('rt_help_content') + '</span></span>',
          preConfirm: () => {
            return {
              text: document.getElementById('content_text').value,
              textWidth: null,
              textHeight: null,
              textX: null,
              textY: null,
              img: null,
              imgWidth: null,
              imgHeight: null,
              imgX: null,
              imgY: null,
              imgRatioLocked: null,
              sound: null,
              soundProperties: null,
              soundLoop: null,
              totalWidth: null,
              totalHeight: null,
              pointCounter: null,
              pointStartX: null,
              pointStartY: null,
              pointEndX: null,
              pointEndY: null,
              pointRotation: null,
              pointSpeed: null,
              pointSize: null,
              flair: null,
              flairSize: null,
              flairTime: null,
              flairIntensity: null
            }
          },
          showLoaderOnConfirm: true
        }, {
          titleText: 'Timing',
          html: '<div style="display: block;">Define Timing</div>' +
            '<input id="timing_totalDuration" class="swal2-input" placeholder="' + l('rt_totalDisplaytime') + '" style="width: 90%;"/><span class="tooltip"><img src="img/fbm/windowRequestDark.png" alt="?" width="24" height="24"><span class="tooltiptext">' + l('rt_help_totalDuration') + '</span></span>' +
            '<input id="timing_openDuration" class="swal2-input" placeholder="' + l('rt_openDisplaytime') + '" style="width: 90%;"/><span class="tooltip"><img src="img/fbm/windowRequestDark.png" alt="?" width="24" height="24"><span class="tooltiptext">' + l('rt_help_openDuration') + '</span></span>' +
            '<input id="timing_miniDuration" class="swal2-input" placeholder="' + l('rt_miniDisplaytime') + '" style="width: 90%;"/><span class="tooltip"><img src="img/fbm/windowRequestDark.png" alt="?" width="24" height="24"><span class="tooltiptext">' + l('rt_help_miniDuration') + '</span></span>',
          preConfirm: () => {
            return {
              delay: null,
              totalDuration: document.getElementById('timing_totalDuration').value,
              openDuration: document.getElementById('timing_openDuration').value,
              miniDuration: document.getElementById('timing_miniDuration').value,
              requestable: null,
              reqPointer: null,
              openCallback: null,
              closeCallback: null
            }
          },
          showLoaderOnConfirm: true
        }, {
          titleText: 'Style',
          html: '<div style="display: block;">Define Style</div>' +
            '<input id="style-theme" class="swal2-input" placeholder="' + l('rt_theme') + '" value="Dark" style="width: 90%;" /><span class="tooltip"><img src="img/fbm/windowRequestDark.png" alt="?" width="24" height="24"><span class="tooltiptext">' + l('rt_help_theme') + '</span></span>' +
            '<input id="style-buttonColor" class="swal2-input" placeholder="' + l('rt_buttonColor') + '" value="#FFFFFF" style="width: 90%;" /><span class="tooltip"><img src="img/fbm/windowRequestDark.png" alt="?" width="24" height="24"><span class="tooltiptext">' + l('rt_help_buttonColor') + '</span></span>',
          preConfirm: () => {
            return {
              theme: document.getElementById('style-theme').value,
              color: null,
              buttonColor: document.getElementById('style-buttonColor').value,
              curve: null,
              visibility: null,
              focus: null,
              windowtype: null,
              fontType: null,
              fontSize: null,
              fontColor: null,
              textAlign: null,
              volume: null,
              screenLocked: null
            }
          },
          showLoaderOnConfirm: true
        }]).then(function(result) {
          return new Promise(function(resolve, reject) {
            if (result.value) {

              var helpText = new createjs.Text(l('rt_pickPointForFeedback'), '12px Arial', 'white');
              helpText.x = 100;
              helpText.y = 100;
              var bounds = helpText.getBounds();
              var shape = new createjs.Shape();
              shape.graphics.beginFill("#eeee").drawRoundRect(100, 100, bounds.width + 10, bounds.height + 10, 10);
              teacherView.getToolbarStage().addChild(shape);
              teacherView.getToolbarStage().addChild(helpText);
              teacherView.getToolbarStage().update();

              function pickPosition(event) {
                MTLG.getStage().removeEventListener('stagemouseup', pickPosition);

                var x = event.stageX / MTLG.getStage().canvas.width;
                var y = event.stageY / MTLG.getStage().canvas.height;
                result.value[0].x = x;
                result.value[0].y = y;

                teacherView.getToolbarStage().removeChild(shape);
                teacherView.getToolbarStage().removeChild(helpText);
                resolve(result);
              }

              MTLG.getStage().addEventListener('stagemouseup', pickPosition);
            } else {
              if (result.dismiss) resolve(result);
              reject(result);
            }
          });

        }, function(err) {
          console.error(l('rt_error:feedbackDialog'), err);
        });
      } catch (e) {
        console.error(l('rt_error:swal'), e);
      }
    }

    return {
      draw: drawCreationModal
    };
  }


  /**
   * feedbackController - self invoking expression.
   *
   * @return {type}           Returns an object with all public functions
   * <pre><code>{
   * teacher: {
   *  giveFeedback: startCreation
   * },
   * player: {
   *  allowFeedback: allowFeedback
   * }</code></pre>
   */
  var feedbackController = (function() {


    /**
     * startCreation - This funciton creates the view for the remote teacher and sends, when the inforamtion is collected, the information to the given player.
     *
     * @param  {type} playerId Receiver of the information for feedback.
     */
    var startCreation = function(playerId) {
      var view = feedbackView();
      //activate the dialog for collecting the inforamtion
      view.draw().then(function(result) {
        if (result.value) {
          MTLG.distributedDisplays.communication.sendMessage(playerId, {
            action: "customAction",
            identifier: 'giveFeedback',
            parameters: [MTLG.distributedDisplays.rooms.getClientId(), result.value]
          });

          swal({
            type: 'success',
            title: l('rt_title:feedbackSent'),
            text: l('rt_text:feedbackSent')
          });
        }
      }, function(err) {
        console.error(l('rt_error:feedbackSent'), err);
        swal({
          type: 'error',
          title: l('rt_title:feedbackError'),
          text: l('rt_error:feedbackSent') + JSON.stringify(err)
        });
      })
    }


    /**
     * generateFeedback - This function generates and renders the feedback on the basis of the given information.
     *
     * @param  {type} from     Sender of the feedback information
     * @param  {type} feedback the information for the feedback
     * @return {type}          description
     */
    var generateFeedback = function(from, feedback) {
      //create target object
      // TODO: Here the code has to be adapted to take a given object as the target
      // and not only an absolute position.
      var target = MTLG.utils.fbm.createTarget("remoteteacher_target");
      target.type = "Target";
      target.name = 'remoteteacher_target';
      target.x = feedback[0].x || 0.5;
      target.y = feedback[0].y || 0.5;
      target.rotation = feedback[0].rotation || 0;
      target.reqPointerRotation = feedback[0].reqPointerRotation || null;

      //create content object
      var content = MTLG.utils.fbm.createContent("remoteteacher_content");
      content.type = "Content";
      content.name = "remoteteacher_content";
      content.text = feedback[1].text || "Hallo";
      content.textWidth = 1;
      content.textHeight = 1;
      content.textX = 0.5;
      content.textY = 0.5;
      content.img = null;
      content.imgWidth = 1;
      content.imgHeight = 1;
      content.imgX = 0.5;
      content.imgY = 0.5;
      content.imgRatioLocked = true;
      content.sound = null;
      content.soundProperties = null;
      content.soundLoop = false;
      content.totalWidth = 0.1;
      content.totalHeight = 0.1;
      content.pointCounter = null; //not yet supported in remoteteacher
      content.pointStartX = null;
      content.pointStartY = null;
      content.pointEndX = null;
      content.pointEndY = null;
      content.pointRotation = 0;
      content.pointSpeed = null;
      content.pointSize = null;
      content.flair = null; //flair does not make sense, because we don't have objects
      content.flairSize = 25;
      content.flairTime = 0.5;
      content.flairIntensity = 1;

      //create timing object
      var timing = MTLG.utils.fbm.createTiming("remoteteacher_timing");
      timing.type = "Timing";
      timing.name = "remoteteacher_timing";
      timing.delay = 1;
      timing.totalDuration = feedback[2].totalDuration || 0;
      timing.openDuration = feedback[2].openDuration || 0;
      timing.miniDuration = feedback[2].minilDuration || 0;
      timing.requestable = true;
      timing.reqPointer = false;
      timing.closeable = feedback[0].closeable === true ? true : false;
      timing.minimizable = feedback[0].minimizable === true ? true : false;
      timing.movable = feedback[0].movable || false;
      timing.movableMini = feedback[0].movableMini || false;
      timing.rotateOnStart = false;
      timing.rotateOnMove = feedback[0].rotateOnMove || false;
      timing.openCallback = null;
      timing.closeCallback = null;

      //create style object
      var style = MTLG.utils.fbm.createStyle("remoteteacher_style");
      style.type = "Style";
      style.name = "remoteteacher_style";
      style.theme = feedback[3].theme || 'Dark';
      style.color = "#FFFFFF";
      style.buttonColor = feedback[3].buttonColor || "#FFFFFF";
      style.curve = 0.5;
      style.visibility = true;
      style.focus = false;
      style.windowtype = 'window';
      style.fontType = 'Arial';
      style.fontSize = null;
      style.fontColor = '#000000';
      style.textAlign = 'left';
      style.volume = 1;
      style.screenLocked = true;

      //create the feedback object
      var feedbackObj = MTLG.utils.fbm.giveFeedback("remoteteacher_target", "remoteteacher_content", "remoteteacher_timing", "remoteteacher_style");
    }

    /**
     * allowFeedback - This function activates the receiving of the feedback information.
     *
     */
    var allowFeedback = function() {
      MTLG.distributedDisplays.actions.setCustomFunction('giveFeedback', generateFeedback);
    }

    return {
      teacher: {
        giveFeedback: startCreation
      },
      player: {
        allowFeedback: allowFeedback
      }
    };
  })();

  return {
    getToolbarElement: addFeedbackToToolbar
  }
})();

/*
 * toolbarFunctionDrawing - self-invoking expression.
 *
 * @return {Object}            Returns an object with all global functions of the drawing
 * <pre><code>{
 * getToolbarElement: buttonFunction
 * }</code></pre>
 */
var toolbarFunctionDrawing = (function() {

  /**
   * buttonFunction - This function creates a button for the remoteteacher toolbar,
   * a callback for closing the drawing and
   * a callback for activating the receiving of the drawing for the player side.
   * If no playerId is given, the function will return [null, null, allowfunction].
   *
   * @param  {type} playerId     The id of the player, who should get the drawing.
   * @param  {type} buttonHeight The height for the button
   * @return {type}              [button, openCallback, closeCallback, Controller.player.allowDrawing, moveStartCallback, moveEndCallback]
   */
  var buttonFunction = function(playerId, buttonHeight) {
    //if no playerId is give, only the allow function is requested.
    if (!playerId) {
      return [null, null, null, Controller.player.allowDrawing];
    }
    var toggle = true;
    var button = createButton(buttonHeight, l('rt_drawing'), 'blue');
    var openCallback = function() {
      if (toggle) {
        Controller.teacher.start(playerId);
        toggle = false;
      } else {
        if (!Controller.teacher.stop(false)) {
          Controller.teacher.start(playerId);
          toggle = false;
        } else {
          toggle = true;
        }
      }
    };

    var closeCallback = Controller.teacher.stop;
    return [button, openCallback, closeCallback, Controller.player.allowDrawing, Controller.teacher.endDraw, Controller.teacher.draw];
  };

  /**
   * drawingView - The view object for the drawing. This creates a new toolbar for the drawing tools.
   *
   */
  var drawingView = function() {
    var _drawingStage;;
    var drawingToolsContainer;


    /**
     * getStage - This function creates a new stage, with a new canvas Element and returns it.
     *
     * @return {Stage}  Returns the stage for the drawing.
     */
    this.getStage = function() {
      if (!_drawingStage) {
        //create canvas for drawing
        let drawingCanvas = MTLG.getStage().canvas.cloneNode(true);
        drawingCanvas.setAttribute('id', 'remoteteacher_drawing');
        MTLG.getStage().canvas.parentNode.insertBefore(drawingCanvas, MTLG.getStage().canvas.nextSibling);

        //Create new stage for drawing
        _drawingStage = new createjs.Stage(drawingCanvas);
        createjs.Touch.enable(_drawingStage);
        _drawingStage.enableMouseOver();
        _drawingStage.nextStage = MTLG.getStage();
        if (teacherView.getToolbarStage()) teacherView.getToolbarStage().nextStage = _drawingStage;
        else MTLG.menu.getMenuStage().nextStage = _drawingStage;

        window.addEventListener('resize', resizeStage, false);
      }
      return _drawingStage;
    }

    var resizeStage = function() {
      var canvas = _drawingStage.canvas;
      let scaleX = window.innerWidth / MTLG.getOptions().width;
      let scaleY = window.innerHeight / MTLG.getOptions().height;
      if (scaleX < scaleY) {
        canvas.width = window.innerWidth;
        canvas.height = MTLG.getOptions().height * scaleX;
      } else {
        canvas.width = MTLG.getOptions().width * scaleY;
        canvas.height = window.innerHeight;
      }

      resizeChildren(_drawingStage);
      _drawingStage.update();
    }


    /**
     * this.closeDrawing - This function hides the drawing toolbar and makes the controller remove the EventListener
     *
     * @return {type}  returns true, if the function runs correctly, false if the drawing was already closed
     */
    this.closeDrawing = function() {
      Controller.teacher.endDraw();
      if (_drawingStage) {
        //if the stage is already not visible, the method should return false
        if (!_drawingStage.visible) return false;
        _drawingStage.visible = false;
        for (var child in _drawingStage.children) {
          _drawingStage.children[child].visible = false;
        }
        _drawingStage.update();
      } else {
        return false;
      }
      drawingToolsContainer.visible = false;
      teacherView.getToolbarStage().update();

      return true;
    }

    /**
     * this.drawDrawingTools - This function draws the toolbar for the drawing tools.
     * It includes a close button, a color picker, a size picker, a path eraser
     * and a delete button for deleting the whole drawing.
     *
     */
    this.drawDrawingTools = function() {
      var buttonWidth = 40;
      var buttonHeight = 40;
      if (!_drawingStage) this.getStage();
      if (!drawingToolsContainer) {
        drawingToolsContainer = new createjs.Container();

        var containerBackground = new createjs.Shape();
        var paddingToolbar = 5;
        var x = paddingToolbar;
        var y = paddingToolbar;
        var width = 0;

        //the close button is for closing the drawing toolbar
        var closeButton = createImageButton(buttonWidth, buttonHeight, 'img/remoteteacher/close.png');
        (function(closeDrawing) {
          closeButton.on('pressup', function() {
            if (!drawingToolsContainer.wasMoved) closeDrawing();
          });
        })(this.closeDrawing);
        closeButton.x = x;
        closeButton.y = y;
        drawingToolsContainer.addChild(closeButton);
        y += closeButton.getBounds().height;
        if (closeButton.getBounds().width > width) width = closeButton.getBounds().width;

        //with the color button, a color can be picked.
        var colorButton = createButton(buttonHeight, ' ', Controller.teacher.getColor(), undefined, buttonWidth);
        //Container for the color picker
        var colorContainer = (function(colorButton) {
          var size = buttonHeight;
          var padding = 5;
          var colors = config.teacherToolsOptions.drawing.colors || ['black', 'white', 'red', 'blue', 'green', 'yellow'];
          var count = colors.length;
          //calculate how many colors should be next to each other
          var numberHorizontal = parseInt(Math.sqrt(count - 1) + 1);
          var numberVertical = Math.ceil(count / numberHorizontal);
          var container = new createjs.Container();

          //create Button for every color
          colors.forEach(function(color, index) {
            var cButton = createButton(size, ' ', color, undefined, size);
            cButton.x = (index % numberHorizontal) * size + padding;
            cButton.y = parseInt(index / numberHorizontal) * size + padding;
            cButton.name = color;
            cButton.on('click', function() {
              Controller.teacher.setColor(this.name);
              //Search for background to fill it in the current color
              for (var index in colorButton.children) {
                if (colorButton.children[index].name === "back") {
                  var bounds = colorButton.getBounds();
                  colorButton.children[index].graphics.clear().beginFill(this.name).drawRoundRect(0, 0, bounds.width, bounds.height, bounds.height / 10);
                  break;
                }
              }
              container.visible = false;
              teacherView.getToolbarStage().update();
            });
            container.addChild(cButton);
          });
          var containerWidth = numberHorizontal * size + 2 * padding;
          var containerHeight = numberVertical * size + 2 * padding;
          var back = new createjs.Shape();
          back.name = "back";
          back.graphics.beginFill('white').drawRoundRect(0, 0, containerWidth, containerHeight, 10);
          container.addChildAt(back, 0);
          container.setBounds(0, 0, containerWidth, containerHeight);
          container.visible = false;
          teacherView.getToolbarStage().addChild(container);
          return container;
        })(colorButton);
        colorButton.on('pressup', function() {
          if (!drawingToolsContainer.wasMoved) {
            //set x and y for colorContainer, because it should be shown on the left hand side of the toolbar if possible
            if ((drawingToolsContainer.x + this.x + width + colorContainer.getBounds().width) < _drawingStage.canvas.width) colorContainer.x = drawingToolsContainer.x + this.x + width;
            else colorContainer.x = drawingToolsContainer.x + this.x - colorContainer.getBounds().width;
            colorContainer.y = this.y + drawingToolsContainer.y;
            colorContainer.visible = true;
            teacherView.getToolbarStage().update();
          }
        });
        colorButton.x = x;
        colorButton.y = y;
        drawingToolsContainer.addChild(colorButton);
        y += colorButton.getBounds().height;
        if (colorButton.getBounds().width > width) width = colorButton.getBounds().width;

        // size of brush
        var sizeButton = createImageButton(buttonWidth, buttonHeight, 'img/remoteteacher/size.png');
        var sizeContainer = (function() {
          var sizeOfButton = 50;
          var padding = 5;
          var sizes = config.teacherToolsOptions.drawing.sizes || ['1', '5', '7', '10', '15', '20'];
          var count = sizes.length;
          //calculate how many colors should be next to each other
          var numberHorizontal = parseInt(Math.sqrt(count - 1) + 1);
          var numberVertical = Math.ceil(count / numberHorizontal);
          var container = new createjs.Container();
          sizes.forEach(function(size, index) {
            var sButton = createButton(sizeOfButton, ' ', 'white', undefined, sizeOfButton);
            sButton.x = (index % numberHorizontal) * sizeOfButton + padding;
            sButton.y = parseInt(index / numberHorizontal) * sizeOfButton + padding;
            sButton.name = size;
            sButton.on('click', function() {
              Controller.teacher.setSize(this.name);
              container.visible = false;
              teacherView.getToolbarStage().update();
            });
            var sizeCircle = new createjs.Shape();
            sizeCircle.graphics.beginFill('black').drawCircle(sizeOfButton / 2, sizeOfButton / 2, size);
            sButton.addChild(sizeCircle);
            container.addChild(sButton);
          });
          var containerWidth = numberHorizontal * sizeOfButton + 2 * padding;
          var containerHeight = numberVertical * sizeOfButton + 2 * padding;
          var back = new createjs.Shape();
          back.name = "back";
          back.graphics.beginFill('white').drawRoundRect(0, 0, containerWidth, containerHeight, 10);
          container.addChildAt(back, 0);
          container.setBounds(0, 0, containerWidth, containerHeight);
          container.visible = false;
          teacherView.getToolbarStage().addChild(container);
          return container;
        })();
        sizeButton.on('pressup', function() {
          if (!drawingToolsContainer.wasMoved) {
            //set x and y for sizeContainer, because it should be shown on the left hand side of the toolbar if possible
            if ((drawingToolsContainer.x + this.x + width + sizeContainer.getBounds().width) < _drawingStage.canvas.width) sizeContainer.x = drawingToolsContainer.x + this.x + width;
            else sizeContainer.x = drawingToolsContainer.x + this.x - sizeContainer.getBounds().width;
            sizeContainer.y = this.y + drawingToolsContainer.y;
            sizeContainer.visible = true;
            teacherView.getToolbarStage().update();
          }
        });
        sizeButton.x = x;
        sizeButton.y = y;
        drawingToolsContainer.addChild(sizeButton);
        y += sizeButton.getBounds().height;
        if (sizeButton.getBounds().width > width) width = sizeButton.getBounds().width;

        var eraseButton = createImageButton(buttonWidth, buttonHeight, 'img/remoteteacher/eraser.png');
        var backgroundErase = new createjs.Shape();
        backgroundErase.name = "back";
        eraseButton.addChildAt(backgroundErase, 0);

        //toggle for activate and deactivate eraser
        var toggle = true;
        eraseButton.on('pressup', function() {
          if (!drawingToolsContainer.wasMoved) {
            //higlight back, if the eraser is activated
            var color = 'rgba(255,255,255,0.5)';
            for (var index in this.children) {
              if (this.children[index].name === "back") {
                var bounds = this.getBounds();
                if (toggle) this.children[index].graphics.beginFill(color).drawRoundRect(0, 0, bounds.width, bounds.height, bounds.height / 5);
                else this.children[index].graphics.clear();
                break;
              }
            }
            //activate eraser or activate drawing
            if (toggle) Controller.teacher.eraseDrawing();
            else Controller.teacher.draw(true);

            toggle = !toggle;
          }
        });
        eraseButton.x = x;
        eraseButton.y = y;
        drawingToolsContainer.addChild(eraseButton);
        y += eraseButton.getBounds().height;
        if (eraseButton.getBounds().width > width) width = eraseButton.getBounds().width;

        //for deleting the whole drawing
        var clearButton = createImageButton(buttonWidth, buttonHeight, 'img/remoteteacher/new.png');
        clearButton.on('pressup', function() {
          if (!drawingToolsContainer.wasMoved) Controller.teacher.clearDrawing();
        });
        clearButton.x = x;
        clearButton.y = y;
        drawingToolsContainer.addChild(clearButton);
        y += clearButton.getBounds().height;
        if (clearButton.getBounds().width > width) width = clearButton.getBounds().width;

        width += 2 * paddingToolbar; //twice padding because of left padding and right padding
        containerBackground.graphics.beginFill('rgba(124,149,232,0.5)').drawRoundRect(0, 0, width, y + 10, 10);
        drawingToolsContainer.addChildAt(containerBackground, 0);
        drawingToolsContainer.setBounds(0, 0, width, y + 10);
        makeContainerMovable(drawingToolsContainer, {
          x: 0,
          y: 0,
          width: _drawingStage.canvas.width,
          height: _drawingStage.canvas.height
        }, Controller.teacher.endDraw, Controller.teacher.draw);

        teacherView.getToolbarStage().addChild(drawingToolsContainer);
        teacherView.getToolbarStage().update();
      } else {
        //show drawingToolsContainer
        drawingToolsContainer.visible = true;
        teacherView.getToolbarStage().update();
        //show drawings
        if (_drawingStage) {
          _drawingStage.visible = true;
          for (var child in _drawingStage.children) {
            _drawingStage.children[child].visible = true;
          }
          _drawingStage.update();
        }
      }
    }
  }

  /**
   * var Controller - self-invoking expression. This is the controller for the drawing.
   * It handles everything concerning the drawing.
   *
   * @return {type}           returns an object with all public methods
   * <pre><code>{
   * teacher: {
   *  start: start,
   *  stop: stop,
   *  sendDrawing: sendDrawing,
   *  clearDrawing: clearDrawing,
   *  eraseDrawing: eraseDrawing,
   *  draw: draw,
   *  endDraw: endDraw,
   *  setColor: setColor,
   *  getColor: getColor,
   *  setSize: setSize
   * },
   * player: {
   *  allowDrawing: allowDrawing
   * }
   *}</code></pre>
    */
  var Controller = (function() {
    var playerId; //the id of the client, who should get the drawing
    var view; //the drawing view
    var stage; //stage for drawing

    var shapeToDraw; //the shape for the current drawing path

    var mouseIsDown = false;
    var erase = false;
    var color, size;
    var oldX, oldY, oldMidX, oldMidY;
    var drawingActive = false;

    /**
     * start - this function initializes the drawing controller.
     *
     * @param  {string} _playerId the player, who should get the drawing
     */
    var start = function(_playerId) {
      playerId = _playerId;
      mouseIsDown = false;
      erase = false;
      color = config.teacherToolsOptions.drawing.colors[0] || 'red';
      size = config.teacherToolsOptions.drawing.sizes[1] || 5;
      if (!view) view = new drawingView();
      view.drawDrawingTools();
      stage = view.getStage();

      //Add EventListener
      draw();
    }

    /**
     * stop - this function stops the drawing function and clears the drawing, so all drawings will be deleted, except if the parameter is false
     *
     * @param  {type} clear = true if this parameter is false, the drawing will not be deleted
     * @return {type}              returns false, if the drawing function was already hidden (non-essential stopped). Even if the function returns false, the drawing will still be cleared
     */
    var stop = function(clear = true) {
      if (stage) {
        if (clear) clearDrawing();
        draw();
        return view.closeDrawing();
      }
    }

    /**
     * handleMouseDown - This function starts the drawing path.
     *
     * @return {type}  description
     */
    var handleMouseDown = function() {
      mouseIsDown = true;
      shapeToDraw = new createjs.Shape();
      shapeToDraw.graphics.beginStroke(color);
      shapeToDraw.mouseEnabled = true;

      //better would be mouseover instead of click, but that does not work with easel 0.8
      shapeToDraw.on('click', function() {
        if (erase) {
          MTLG.distributedDisplays.communication.sendSharedObjectDeletion(playerId, this.sharedId);
          this.parent.removeChild(this);
          stage.update();
        }
      });

      oldX = stage.mouseX;
      oldY = stage.mouseY;

      oldMidX = stage.mouseX;
      oldMidY = stage.mouseY;
    };

    /**
     * handleMouseMove - This function draws the actual drawing path
     *
     */
    var handleMouseMove = function() {
      if (mouseIsDown) {
        //creates a point for a more realistic not edgy path
        var pt = new createjs.Point(stage.mouseX, stage.mouseY);
        var midPoint = new createjs.Point(oldX + pt.x >> 1, oldY + pt.y >> 1);
        shapeToDraw.graphics.ss(size, "round", "round").beginStroke(color).moveTo(midPoint.x, midPoint.y).curveTo(oldX, oldY, oldMidX, oldMidY);

        //save old coordinates for the next curve.
        oldX = pt.x;
        oldY = pt.y;

        oldMidX = midPoint.x;
        oldMidY = midPoint.y;

        stage.addChild(shapeToDraw);
        stage.update();
      }
    };

    /**
     * handleMouseUp - This function ends the drawing path and sends it to the client
     *
     */
    var handleMouseUp = function() {
      mouseIsDown = false;
      sendDrawing();

      shapeToDraw = null;
    };

    /**
     * var sendDrawing - This function actual sends the drawing to the client
     *
     */
    var sendDrawing = function() {
      if (shapeToDraw !== null) {
        var sharedObj = new MTLG.distributedDisplays.SharedObject(shapeToDraw, false, true);
        //set syncProperties
        sharedObj.syncProperties.scaleX = {
          send: true,
          receive: false
        };
        sharedObj.syncProperties.scaleY = {
          send: true,
          receive: false
        };
        sharedObj.syncProperties.x = {
          send: true,
          receive: false
        };
        sharedObj.syncProperties.y = {
          send: true,
          receive: false
        };
        sharedObj.init();

        var send = sharedObj.toSendables();
        var bounds = {
          x: stage.canvas.clientLeft,
          y: stage.canvas.clientTop,
          width: stage.canvas.width,
          height: stage.canvas.height
        }
        MTLG.distributedDisplays.communication.sendMessage(playerId, {
          action: "customAction",
          identifier: 'sendDrawing',
          addServerListener: send,
          parameters: [MTLG.distributedDisplays.rooms.getClientId(), send, bounds]
        });
      }
    };

    /**
     * clearDrawing - This function is for clearing the screen and delete all drawings
     *
     */
    var clearDrawing = function() {
      erase = false;
      shapeToDraw = null;
      if (!stage) return; //if the stage is not defined, the drawing tool is not initialized
      for (var index in stage.children) {
        if (stage.children[index].sharedId) {
          MTLG.distributedDisplays.communication.sendSharedObjectDeletion(playerId, stage.children[index].sharedId);
        }
      }
      stage.removeAllChildren();
      stage.update();
    };

    /**
     * eraseDrawing - This function enables the click to delete a drawing path
     *
     */
    var eraseDrawing = function() {
      erase = true;
      //no Longer drawing
      endDraw();
    }

    /**
     * draw - Enables Listener to draw on the screen
     *
     * @param  {type} toggleErase = false True if the erase boolean should be toggled, so that if erase was on, it is now off.
     */
    var draw = function(toggleErase = false) {
      if (toggleErase) erase = !erase;
      if (erase) return;
      if (!stage) return;
      if (drawingActive) return;
      stage.addEventListener('stagemousedown', handleMouseDown);
      stage.addEventListener('stagemousemove', handleMouseMove);
      stage.addEventListener('stagemouseup', handleMouseUp);
      drawingActive = true;
    }

    /**
     * endDraw - Disables Listener to prevent drawing.
     *
     */
    var endDraw = function() {
      if (!stage) return;
      //Remove EventListener
      stage.removeEventListener('stagemousedown', handleMouseDown);
      stage.removeEventListener('stagemousemove', handleMouseMove);
      stage.removeEventListener('stagemouseup', handleMouseUp);
      drawingActive = false;
    }

    /**
     * showDrawing - This function is for showing the recieved drawing. It automatically scales the drwaing.
     *
     * @param  {type} from    The sender.
     * @param  {type} drawing The drawing to be shown
     * @param  {type} bounds  The bounds of the sender screen.
     */
    var showDrawing = function(from, drawing, bounds) {
      //create stage, if does not already exists
      if (!view) view = new drawingView();
      if (!stage) {
        if (!view) return;
        stage = view.getStage();
        createjs.Ticker.addEventListener('tick', stage);

        //Remove from stage after deletion
        MTLG.distributedDisplays.actions.registerListenerForAction("deleteSharedObject", function({sharedId}) {
          stage.children = stage.children.filter(c => c.sharedId != sharedId);
        });
      }

      // Get the sharedId of the drawingContainer from the associative list
      var sharedId;
      for (var i in drawing) {
        if (drawing[i].sharedId) {
          sharedId = drawing[i].sharedId;
          break;
        }
      }
      try {
        MTLG.distributedDisplays.actions.addSharedObjects(drawing);
        var sharedContainer = MTLG.distributedDisplays.SharedObject.getSharedObject(sharedId);

        //scale
        //Because the window of the sender could have other bounds, the scale has to be calculated, otherwise the original scale of the sender is used.
        var scaleX = stage.canvas.width / bounds.width;
        var scaleY = stage.canvas.height / bounds.height;

        // calculate new x and y coordinates, because the scale is based on the center and not the zero-point
        var newX = (scaleX * sharedContainer.createJsObject.x / sharedContainer.createJsObject.scaleX);
        var newY = (scaleY * sharedContainer.createJsObject.y / sharedContainer.createJsObject.scaleY);

        sharedContainer.createJsObject.scaleX = scaleX;
        sharedContainer.createJsObject.scaleY = scaleY;
        sharedContainer.createJsObject.x = newX;
        sharedContainer.createJsObject.y = newY;

        //Because these are not the same Stages, the sharedObject has parent "None" and we have to manually add the container to the new stage
        stage.addChild(sharedContainer.createJsObject);
        stage.update();
      } catch (e) {
        console.error(l('error:addSharedObjects'));
      }
    }

    /**
     * setColor - sets the color of the current brush and activates the drawing, if not already happened.
     *
     * @param  {String} newColor The new color. This can be any legit CSS color.
     */
    var setColor = function(newColor) {
      color = newColor;
      draw();
    }

    /**
     * getColor - Gives the current color of the brush.
     *
     * @return {String}  Returns a CSS string of color.
     */
    var getColor = function() {
      return color;
    }

    /**
     * setSize - sets the size of the current brush and activates drawing, if not already happened.
     *
     * @param  {Number} newSize The new size. This can be any positive number.
     */
    var setSize = function(newSize) {
      if (newSize < 0) {
        console.error(l('error:NovalidSize'));
        return;
      }
      size = newSize;
      draw();
    }

    /**
     * allowDrawing - This function is for enable receiving the drawing and enables the showing function.
     *
     */
    var allowDrawing = function() {
      MTLG.distributedDisplays.actions.setCustomFunction('sendDrawing', showDrawing);
    }

    return {
      teacher: {
        start: start,
        stop: stop,
        sendDrawing: sendDrawing,
        clearDrawing: clearDrawing,
        eraseDrawing: eraseDrawing,
        draw: draw,
        endDraw: endDraw,
        setColor: setColor,
        getColor: getColor,
        setSize: setSize
      },
      player: {
        allowDrawing: allowDrawing
      }
    }
  })();

  return {
    getToolbarElement: buttonFunction
  };
})();

/********************************************************** standard role allocation ********************************************************/

/*
 * check_remoteteacherStart - This function checks the gamesState and activates the remoteteacher start level.
 *
 * @param  {type} gameState description
 * @return {type}           description
 */
var check_remoteteacherStart = function(gameState) {
  if (!gameState) {
    console.log("gameState undefined");
    return 1; //assuming we should start
  }
  if (!gameState.nextLevel) {
    console.log("gameState.nextLevel not set");
    return 1; //assuming we should start
  }
  if (gameState.nextLevel === 0) {
    return 1;
  } else {
    return 0;
  }
}

/*
 * init - Initializes the remote teacher module.
 *
 * @param  {type} options Options for the remote teacher module as an Object like { remoteteacher: {}}
 */
var init = function(options) {
  //set Options
  if (options.remoteteacher) {
    let _config = {}
    Object.assign(_config,                     config,                     options.remoteteacher)
    Object.assign(_config.teacherTools,        config.teacherTools,        options.remoteteacher.teacherTools);
    Object.assign(_config.teacherToolsOptions, config.teacherToolsOptions, options.remoteteacher.teacherToolsOptions);
    config = _config
  }

  //copy dimensions for more convenient syntax
  _width = MTLG.getOptions().width / 100.0;
  _height = MTLG.getOptions().height / 100.0;

  //test, if dd is added to mtlg
  if (!MTLG.distributedDisplays) console.error(l('rt_error:noDDM'));

  //Add all functions for active intervention
  if (config.teacherTools.feedback && MTLG.utils.fbm) toolbarFunctions.set('feedback', toolbarFunctionFeedback.getToolbarElement);
  else console.log('No feedback module installed');
  if (config.teacherTools.drawing) toolbarFunctions.set('drawing', toolbarFunctionDrawing.getToolbarElement);

  //set globalScale for multiObserver, because the number of players, who should be shown in multiObserver, can be defined in the options
  config.globalScale = 1 / (parseInt(Math.sqrt(config.numberOfPlayersForMultiObserver - 1)) + 1);


  if (config.teacherDefaultScreen) {
    //register the room selection as a level
    MTLG.lc.registerLevel(MTLG.remoteteacher.playerView.draw, check_remoteteacherStart, MTLG.remoteteacher.resize);
    //add the submenu, where the teacher can go to the teacherScreen
    MTLG.menu.addSubMenu(teacherView.drawRoomHandler);
  }

  console.log('Init Remote teacher TEST');
};

MTLG.remoteteacher = {
  init,
  teacherView,
  playerView,
  resize
};

MTLG.addModule(init);

