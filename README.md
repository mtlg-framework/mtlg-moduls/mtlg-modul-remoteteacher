## What is the remoteteacher module
The remoteteacher module is an extension of the mtlg framework. With this module a teacher can observe the screens of the players. The remoteteacher has an overview of all gamesessions with metainformation, an overview of all screens and the ability to observe a single gamesession.

------------------
## Run demo

Go into the folder "demo"

1) `npm install`

4) start distributedDisplay Server (see <https://gitlab.com/mtlg-framework/mtlg-moduls/mtlg-modul-distributeddisplays>)

5) serve game with `mtlg serve`

6) open browser at <http://localhost:8080>

------------------
## Use of API to include the remoteteacher module in a game.
To use the remoteteacher module you have to install the module.

There are two ways of using the remoteteacher module. You can use the standard role allocation or implement your own.

### Using the standard role allocation
For using the standard role allocation you don't have to change the settings.
To show the rooms to the teacher and players a new level will be added by the remoteteacher module. Because of that you have to change the check function of your first level. By default your first level will be started, but now the remoteteacher level should start, so you have to change these two return values to 0:
```
if (!gameState) {
    console.log("gameState undefined");
    return 0;
  }
  if (!gameState.nextLevel) {
    console.log("gameState.nextLevel not set");
    console.log(gameState);
    return 0;
  }
```

### Using a customized role allocation
To use the customized alternative you have to change the settings of the remoteteacher module. For changing the settings you go to the file manifest/game.config.js and add to the options following code:
```
remoteteacher: {
      teacherDefaultScreen: false
}
```
The teacherView as well as the playerView has to be initialized with the functions `MTLG.remoteteacher.teacherView.init(name, getPlayers)` and  `MTLG.remoteteacher.playerView.init(name)`. `name` is in both functions the clientId or room of the teacher. `getPlayers` is a callback to get all current players to show them to the teacher. The Callback should return a promise.
The function `MTLG.remoteteacher.teacherView.draw()` can be called to draw the mainscreen for the remoteteacher.

### generel
To show metainformation about the game to the remoteteacher you have to define an object. The attributes name is also the display name of the metainformation. The value of the attribute is the value of the metainformation. With the function `MTLG.remoteteacher.playerView.setMetadata(object)`you mark you object as metainformation. Everytime you change this object, the display of the remoteteacher changes, too.
First of each level you have to point out, which elements should be shown to the remoteteacher. You can do this with the function `MTLG.remoteteacher.playerView.forSharing([element1, element2]);`. If you provide `true` as a second parameter, you only add the elements, with an false you replace them, if there already were elements synchronized.